<?php
include_once 'config.php';
include_once 'partials/navbar.php';
include_once 'partials/sidebar.php';
$videos = False;
if (isset($_SESSION["user_id"])) {
    $video_ids = View::getHistory($_SESSION["user_id"]);
    foreach ($video_ids as $video) {
        $videos[] = Video::getById($video);
    }
}
?>
<div class="content d-inline-block float-right bg-grey">
    <div class="container-fluid bg-grey">
        <div class="row">
            <div class="col-lg-9 order-2 order-sm-1">
                <div class="container-fluid bg-grey padded">
                    <div class="row" style="border-bottom: 2px solid #ededed">
                        <div class="col-sm-6"><h6 class="m-3"><i class="fa fa-history"></i> Watch History</h6></div>
                        <div class="col-sm-6 text-right"></div>
                        <?php
                        if ($videos) {
                            foreach ($videos as $video) {
                                include 'partials/history_block.php';
                            }
                        } else {
                            echo '<div class="empty text-center">You have no videos</div>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 history-dark order-1 order-sm-2 side-full d-none d-lg-block">
                <div class="mt-5 pl-3">
                    <i class="fa fa-search mr-2" style="color:#1b1e21"></i> <input type="text" placeholder="Search Watch history" class="history-search" />
                </div>
                <ul class="list-group-flush mt-3 p-0">
                    <li class="list-group-item list-group-item-dark">Today</li>
                    <li class="list-group-item list-group-item-dark">Yesterday</li>
                    <li class="list-group-item list-group-item-dark">Last Week</li>
                    <li class="list-group-item list-group-item-dark">Last Month</li>
                </ul>
                <p><a href=""><b>CLEAR ALL WATCH HISTORY</b></a></p>
            </div>
        </div>
    </div>
    <?php include_once 'partials/footer.php'; ?>

