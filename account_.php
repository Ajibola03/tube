
<?php
include_once 'config.php';
include_once 'helper.php';
include_once 'classes/User.php';
include_once 'classes/Video.php';
include_once 'classes/Playlist.php';
include_once 'classes/Like.php';
include_once 'classes/Channel.php';
include_once 'classes/Guest.php';
include_once 'partials/navbar.php';
include_once 'partials/sidebar.php';
?>
<style type="text/css">
    .content{
        margin-top: 76px;
        width:84%;
        max-height: 90vh;
        overflow-y: auto;
        margin-bottom: 10px;
    }

    .main{
        margin-top: 10px;
        box-shadow: none;
    }

    @media(max-width: 375px){
        .main-raised {
            margin: 0; 
        }
    }
</style>
<body class="profile-page bg-grey">
    <div class="content d-inline-block float-right bg-grey">
        <div class="page-heder heae-filter" data-paralax="true"></div>
        <div class="main main-raised pt-5">
            <div class="bg-white">
                <div class="profile-content">
                    <div class="container">
                        <?php
                        if (!isset($_SESSION["user_id"])) {
                            include_once 'partials/no_channel.php';
                        } else {
                            if (Channel::getByUserId($_SESSION["user_id"])) {
                                $channel = Channel::getByUserId($_SESSION["user_id"]);
                                $user = User::getById($_SESSION["user_id"]);
                                if (isset($_SESSION["move_all"])) {
                                    if ($_SESSION["move_all"] == True) {
                                        Video::moveAll($user->getId(), $channel->getId());
                                        View::moveAll($user->getId(), $channel->getId());
                                    }
                                }
                                $videos = Video::getByChannelId($channel->getId());
                                $playlists = Playlist::getByUserId($_SESSION["user_id"], 2);
                                include_once 'partials/my_channel.php';
                            } else {
                                $user = User::getById($_SESSION["user_id"]);
                                include_once 'partials/no_channel.php';
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer text-center ">

    </footer>

    <!-- jquery -->
    <script src="<?= BASE_URL ?>dist/js/jquery-3.4.1.js"></script>
    <!-- popper -->
    <script src="<?= BASE_URL ?>dist/js/popper.min.js"></script>
    <!-- bootstrap -->
    <script src="<?= BASE_URL ?>bs.v4/js/bootstrap.min.js"></script>
    <script src="<?= BASE_URL ?>plugins/account_/js/account_.js"></script>
    <script src="<?= BASE_URL ?>dist/js/main.js"></script>
    <?php flash("error") ?>
</body>

