<?php
include_once '../../classes/User.php';
include_once '../../classes/Video.php';
include_once '../../partials/admin/navbar.php';
include_once '../../partials/admin/sidebar.php';
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Videos</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <!-- /.card -->

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Videos</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>s/n</th>
                                        <th>Id</th>
                                        <th>User</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Channel</th>
                                        <th>Views</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th>Country</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    $videos = Video::getAll();
                                    foreach ($videos as $video) {
                                        $i++;
                                        include '../../partials/admin/video_row.php';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<?php include_once '../../partials/admin/footer.php'; ?>