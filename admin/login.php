<?php
include_once '../config.php';
include_once '../helper.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AdminLTE 3 | Log in</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= BASE_URL ?>fontawesome/css/all.min.css">
        <link rel="stylesheet" href="<?= BASE_URL ?>fontawesome/css/brands.min.css">
        <link rel="stylesheet" href="<?= BASE_URL ?>fontawesome/css/regular.min.css">
        <link rel="stylesheet" href="<?= BASE_URL ?>fontawesome/css/svg-with-js.css">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?= BASE_URL ?>bs.v4/css/bootstrap.min.css">
        <!-- DataTables -->
        <link rel="stylesheet" href="<?= BASE_URL ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="<?= BASE_URL ?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="<?= BASE_URL ?>plugins/select2/css/select2.min.css">
        <link rel="stylesheet" href="<?= BASE_URL ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
        <!-- Bootstrap4 Duallistbox -->
        <link rel="stylesheet" href="<?= BASE_URL ?>plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
        <!-- DataTables -->
        <link rel="stylesheet" href="<?= BASE_URL ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="<?= BASE_URL ?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= BASE_URL ?>plugins/admin/css/adminlte.min.css">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="../../index2.html"><b>Bloog</b>View</a>
            </div>
            <!-- /.login-logo -->
            <div class="card">
                <div class="card-body login-card-body">
                    <p class="login-box-msg">Login in to start your session</p>

                    <form action="<?= BASE_URL ?>/controllers/admin_login" method="post">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Username" name="user_name">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-user"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="password" class="form-control" placeholder="Password" name="password">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-8">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="remember">
                                    <label for="remember">
                                        Remember Me
                                    </label>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-4">
                                <button type="submit" class="btn btn-primary btn-block">Login</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>

                    <div class="social-auth-links text-center mb-3">
                        <p>- OR -</p>
                        <div href="#" class="btn btn-block btn-primary">
                            <i class="fa fa-users  mr-2"></i> Login in as User
                        </div>
                    </div>
                </div>
                <!-- /.login-card-body -->
            </div>
        </div>
        <!-- /.login-box -->

        <!-- jquery -->
<script src="<?= BASE_URL ?>dist/js/jquery-3.4.1.js"></script>
<!-- popper -->
<script src="<?= BASE_URL ?>dist/js/popper.min.js"></script>
<!-- bootstrap -->
<script src="<?= BASE_URL ?>bs.v4/js/bootstrap.min.js"></script>
        <!-- Select2 -->
        <script src="<?= BASE_URL ?>plugins/select2/js/select2.full.min.js"></script>
        <!-- Bootstrap4 Duallistbox -->
        <script src="<?= BASE_URL ?>plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
        <!-- SweetAlert2 -->
        <script src="<?= BASE_URL ?>plugins/sweetalert2/sweetalert2.min.js"></script>
        <!-- Toastr -->
        <script src="<?= BASE_URL ?>plugins/toastr/toastr.min.js"></script>
        <script type="text/javascript">
            function flash(type, message) {
                if (type && message) {
                    if (type === "success") {
                        toastr.success(message);
                    } else if (type === "info") {
                        toastr.info(message);
                    } else if (type === "danger") {
                        toastr.error(message);
                    } else if (type === "error") {
                        toastr.error(message);
                    } else if (type === "warning") {
                        toastr.warning(message);
                    } else {
                        toastr.warning(message);
                    }
                }
            }
        </script>
        <!-- AdminLTE App -->
<script src="<?= BASE_URL ?>plugins/admin/js/adminlte.js"></script>
        <?php flash("error"); ?>
    </body>
</html>
