<?php
include_once 'config.php';
include_once 'classes/Video.php';
include_once 'classes/Subscription.php';
include_once 'getID3/getid3/getid3.php';

$resolution = [0, 0];
if (isset($_GET['id'])) {
    $video = Video::getById($_GET["id"]);
    $comments = $video->getComments();
    $getID3 = new getID3;
    $path = $video->getPath();
    $file = $getID3->analyze($path);
    if(isset($file['video'])){
    $resolution = [$file['video']["resolution_x"], $file['video']["resolution_y"]];
    }
//    var_dump($resolution);
//    die;
    if ($resolution[1] < 800) {
        $low_res = 0;
    } else {
        $low_res = 1;
    }
}
//var_dump($resolution);
//die;
$ref_id = isset($_GET["ref_id"]) ? $_GET['ref_id'] : null;
$raw_playlist = Video::getAll();
if (isset($_GET['id'])) {
    $playlist = array_remove($raw_playlist, Video::getById($_GET["id"]));
}
include_once 'partials/navbar.php';
include_once 'partials/sidebar.php';
$user = isset($_SESSION["user_id"]) ? User::getById($_SESSION["user_id"]) : False;
if (isset($_SESSION["current_video"]) && isset($_GET['id'])) {
    if ($_SESSION["current_video"] !== $_GET["id"]) {
        $_SESSION["video_id"] = 0;
    }
}
$channel = False;
if (isset($_GET['id'])) {
    if ($video->getChannel()) {
        $channel = Channel::getById($video->getChannelId());
    }
}
?>
<style>
</style>
<div class="content d-inline-flex float-right">
    <div class="container-fluid">
        <div class="d-inline-block float-lg-left vid-body">
            <div id="vid" class=" w-80 m-auto" style="background: black">
                <?php
                if (isset($_SESSION["video_id"])) {
                    if ($_SESSION["video_id"] !== 0) {
                        if ($resolution[1] < 750) {
                            include_once 'partials/vid_row.php';
                        } else {
//                            var_dump($resolution);
//                            die;
                            include_once 'partials/vid_row.php';
                        }
                    } else {
                        include_once 'partials/ad_row.php';
                    }
                } else {
                    include_once 'partials/ad_row.php';
                }
                ?>
            </div>
            <div style="max-width: 100%" class="bg-grey">
                <!--<div class="py-2"></div>-->
                <div class="container-fluid">
                    <div class="row mt-3">
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-1 d-none d-sm-inline-block">
                                    <img src="<?= BASE_URL ?>uploads/profiles/<?= $video->getLogo() ?>" style="width:100%" class="rounded-circle">
                                </div>
                                <div class="col-11">
                                    <div class="row">
                                        <div class="col-12">
                                            <h5><?= $video->getTitle() ?></h5>
                                        </div>
                                        <div id="first" class="details text-center justify-content-around">
                                            <span><?= $video->getViews() ?> views</span>
                                            <?php
                                            if (isset($_SESSION["user_id"])) {
                                                include_once 'partials/user_options.php';
                                            } else {
                                                include_once 'partials/no_user_options.php';
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-9 col-sm-3 details p-3">
                            <div class="float-left" style="display: inline-block">
                                <?php
                                if ($channel) {
                                    if ($user) {
                                        if (Subscription::getSub($_SESSION["user_id"], $video->getChannelId())) {
                                            echo $channel;
                                            include_once 'partials/subscribe_button2.php';
                                        } else {
                                            echo $channel;
                                            include_once 'partials/subscribe_button.php';
                                        }
                                    } else {

                                        echo "<b>" . $channel . "</b>";
                                    }
                                } else {
                                    echo $user;
                                }
                                ?>
                            </div>
                            <?php
//                            if ($channel && $user) {
//                                if (Subscription::getSub($_SESSION["user_id"], $video->getChannelId())) {
//                                    include_once 'partials/subscribe_button2.php';
//                                } else {
//                                    include_once 'partials/subscribe_button.php';
//                                }
//                            }
                            ?>
                        </div>
                        <div class="col-12 mt-2 pt-2" style="border-top: .5px solid grey">
                            <div class="btn btn-sm clickable float-right">show less</div>
                            <div class="clearfix"></div>
                            <pre><?= $video->getDescription() ?></pre>
                        </div>
                        <div class="col-12 mt-2 pt-2 pb-3" style="border-top: .5px solid grey">
                            <label class="label-group">Enter your Comment</label>
                            <div class="input-group">
                                <textarea id="comment" type="text" 
                                          class="form-control comment"
                                          onfocus="$('.buttons').removeClass('d-none')"
                                          placeholder="Enter A Comment">
                                </textarea>
                            </div>
                            <div class="float-right my-2 buttons d-none">
                                <div class="btn btn-danger" onclick="$('.comment').val('')">Cancel</div>
                                <div class="btn btn-primary" 
                                     onclick="comment(<?= $_SESSION['user_id'] ?>,<?= $video->getId() ?>, document.getElementById('comment').value)">Submit</div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row" id="comment-area">
                                <?php
                                foreach ($comments as $comment) {
                                    include 'partials/comment_row.php';
                                }
                                ?>
                            </div>
                        </div>
                        <div class="d-lg-none" style="overflow-y: scroll">
                            <div class="container-fluid">
                                <div class="row " style="position: fixed !important">
                                    <?php
                                    foreach ($playlist as $item) {
                                        include 'partials/playlist_row.php';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="d-none d-lg-inline-block float-right" style="width:20%;">
            <div class="container-fluid">
                <div class="row">
                    <?php
                    foreach ($playlist as $item) {
                        include 'partials/playlist_row.php';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php include_once 'partials/footer.php'; ?>
