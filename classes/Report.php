<?php

include_once 'Connection.php';

class Report extends Connection{
    
    private $id, $video_id, $user_id, $poster_id, $date, $status, $conn;
    
    function __construct($video_id, $user_id, $poster_id, $date, $status = 0, $id = null) {
        $this->id = $id;
        $this->video_id = $video_id;
        $this->user_id = $user_id;
        $this->poster_id = $poster_id;
        $this->date = $date;
        $this->status = $status;
        $this->conn = new Connection;
    }
    
    public function add() {
        $q = "INSERT INTO reports(video_id = ?, user_id = ?, poster_id = ?, date = ?)";
        $params = [$this->video_id, $this->user_id, $this->poster_id, $this->date];
        $result = $this->conn::query($q, $params);
        if ($result){
            return new Report($this->video_id, $this->user_id, $this->poster_id, $this->date, $this->status, $this->id);
        }
        return False;
    }
    
    public function update() {
        $q = "UPDATE reports SET video_id = ?, user_id = ?, poster_id = ?, date = ?, status = ? WHERE id = ?";
        $params = [$this->video_id, $this->user_id, $this->poster_id, $this->date, $this->status, $this->id];
        $result = $this->conn::query($q, $params);
        if ($result){
            return new Report($this->video_id, $this->user_id, $this->poster_id, $this->date, $this->status, $this->id);
        }
        return False;
    }
    
    public static function getAll() {
        $q = "SELECT * FROM reports";
        $conn = new Connection;
        $result = $conn::query($q);
        $reports = [];
        foreach ($result as $value) {
            $reports[] = new Report($value->video_id, $value->user_id, $value->poster_id, $value->date, $value->status, $value->id);
        }
        return $reports;
    }
    
    public static function getByUserId($user_id) {
        $q = "SELECT * FROM reports WHERE user_id = ?";
        $params = [$user_id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        $reports = [];
        foreach ($result as $value) {
            $reports[] = new Report($value->video_id, $value->user_id, $value->poster_id, $value->date, $value->status, $value->id);
        }
        return $reports;
    }
    
    public static function getById($id) {
        $q = "SELECT * FROM reports WHERE id = ?";
        $params = [$id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        $report = False;
        if ($result){
            $report = new Report($this->video_id, $this->user_id, $this->poster_id, $this->date, $this->status, $this->id);
        }
        return $report;
    }
    
    public function getId() {
        return $this->id;
    }

    public function getVideo_id() {
        return $this->video_id;
    }

    public function getUser_id() {
        return $this->user_id;
    }

    public function getPoster_id() {
        return $this->poster_id;
    }

    public function getDate() {
        return $this->date;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setVideoId($video_id) {
        $this->video_id = $video_id;
        return $this;
    }

    public function setUserId($user_id) {
        $this->user_id = $user_id;
        return $this;
    }

    public function setPosterId($poster_id) {
        $this->poster_id = $poster_id;
        return $this;
    }

    public function setDate($date) {
        $this->date = $date;
        return $this;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }


}

