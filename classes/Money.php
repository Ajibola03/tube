<?php

include_once 'Connection.php';
include_once 'User.php';

class Money extends Connection {

    private $user_id, $channel_id, $paypal, $prev_views = 0, $prev_posts = 0, $type, $status = 1, $id = null, $conn;

    function __construct($user_id, $channel_id, $paypal, $type, $prev_views = 0, $prev_posts = 0, $status = 1, $id = null) {
        $this->channel_id = $channel_id;
        $this->user_id = $user_id;
        $this->paypal = $paypal;
        $this->prev_views = $prev_views;
        $this->prev_posts = $prev_posts;
        $this->type = $type;
        $this->status = $status;
        $this->id = $id;
        $this->conn = new Connection;
    }

    public function add() {
        $q = "INSERT INTO money_users(user_id, channel_id, paypal, prev_views, prev_posts, type, status) VALUES (?,?,?,?,?,?,?)";
        $params = [$this->user_id, $this->channel_id, $this->paypal, $this->prev_views, $this->prev_posts, $this->type, $this->status];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return new Money($this->user_id, $this->channel_id, $this->paypal, $this->type, $this->prev_views, $this->prev_posts, $this->status, $this->id);
        }
        return False;
    }

    public function update() {
        $q = "UPDATE money_users SET user_id = ?, channel_id = ?, paypal = ?, prev_views = ?, prev_posts = ?, type = ?, status = ? WHERE id = ?;";
        $params = [$this->user_id, $this->channel_id, $this->paypal, $this->prev_views, $this->prev_posts, $this->type, $this->status, $this->id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return new Money($this->user_id, $this->channel_id, $this->paypal, $this->type, $this->prev_views, $this->prev_posts, $this->status, $this->id);
        }
        return FALSE;
    }

    public static function getById($id) {
        $q = "SELECT * FROM money_users WHERE id = ?";
        $params = [$id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        foreach ($result as $row) {
            $money = new Money($row->user_id, $row->channel_id, $row->paypal, $row->type, $row->prev_views, $row->prev_posts, $row->status, $row->id);
        }
        return $money;
    }

    public static function getByUserId($id) {
        $q = "SELECT * FROM money_users WHERE user_id = ?";
        $params = [$id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        $money = False;
        foreach ($result as $row) {
            $money = new Money($row->user_id, $row->channel_id, $row->paypal, $row->type, $row->prev_views, $row->prev_posts, $row->status, $row->id);
        }
        return $money;
    }

    public static function getByPaypal($paypal) {
        $q = "SELECT * FROM money_users WHERE paypal = ?";
        $params = [$paypal];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        $money = False;
        if ($result) {
            $money = True;
        }
        return $money;
    }

    public function calcRawAmount() {
        switch ($this->type) {
            case 1:
                $per_view = BLOOGER_PER_VIEW;
                break;
            case 2:
                $per_view = BLOOGER_PER_VIEW;
                break;
            case 0:
                $per_view = BLOOGER_PER_VIEW;
                break;
        }

        switch ($this->type) {
            case 0:
                $query = "SELECT COUNT(user_id) AS views FROM views WHERE ad = 1 and user_id = ?";
                break;
            case 1:
                $query = "SELECT COUNT(poster_id) AS views FROM views WHERE ad = 1 and poster_id = ?";
                break;
            case 2:
                $query = "SELECT COUNT(ref_id) AS views FROM views WHERE ad = 1 and ref_id = ?";
                break;
        }
        $params = [$this->user_id];
        $result = $this->conn::query($query, $params);
        if ($result) {
            $sum = $result[0]->views * $per_view;
        }
        return $sum;
    }

    public function getTotalAmountExtracted() {
        switch ($this->type) {
            case 1:
                $per_view = BLOOGER_PER_VIEW;
                break;
            case 2:
                $per_view = BLOOGER_PER_VIEW;
                break;
            case 0:
                $per_view = BLOOGER_PER_VIEW;
                break;
        }
        if ($this->prev_posts == $this->prev_views) {
            $sum = $this->prev_posts * $per_view;
        } else {
            
        }
        return $sum;
    }

    public function getAmountAvl() {
        if ($this->prev_posts == $this->prev_views) {
            $sum = $this->calcRawAmount() - $this->getTotalAmountExtracted();
        } else {
            
        }
        return $sum;
    }

    public function getUser_id() {
        return $this->user_id;
    }

    public function getChannel_id() {
        return $this->channel_id;
    }

    public function getPaypal() {
        return $this->paypal;
    }

    public function getPrevViews() {
        return $this->prev_views;
    }

    public function getPrevPosts() {
        return $this->prev_posts;
    }

    public function getType() {
        return $this->type;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getId() {
        return $this->id;
    }

    public function getConn() {
        return $this->conn;
    }

    public function setUser_id($user_id) {
        $this->user_id = $user_id;
        return $this;
    }

    public function setChannel_id($channel_id) {
        $this->channel_id = $channel_id;
        return $this;
    }

    public function setPaypal($paypal) {
        $this->paypal = $paypal;
        return $this;
    }

    public function setPrevViews($prev_views) {
        $this->prev_views = $prev_views;
        return $this;
    }

    public function setPrevPosts($prev_posts) {
        $this->prev_post = $prev_posts;
        return $this;
    }

    public function setType($type) {
        $this->type = $type;
        return $this;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setConn($conn) {
        $this->conn = $conn;
        return $this;
    }

}
