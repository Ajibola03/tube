<?php

include_once "Connection.php";
include_once "Video.php";
include_once __DIR__.'./'.'../config.php';
include_once  __DIR__.'./'.'../helper.php';

class Playlist extends Connection{
    
    private $id, $name, $user_id, $status, $date, $type, $conn;
    
    function __construct($name, $user_id, $date, $type = 1, $status = 1, $id = null) {
        $this->id = $id;
        $this->name = $name;
        $this->user_id = $user_id;
        $this->date = $date;
        $this->status = $status;
        $this->type = $type;
        $this->conn = new Connection;
    }
    function add() {
        $q = "INSERT INTO playlist(name, user_id, status, date, type) VALUES (?,?,?,?,?)";
        $params = [$this->name, $this->user_id, $this->status, $this->date, $this->type];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return new Playlist($this->name, $this->user_id, $this->date, $this->type, $this->status, $this->id);                    
        }
        return FALSE;
    }

    function update() {
        $q = "UPDATE playlist SET name = ?, user_id = ?, status = ? WHERE id = ?";
        $params = [$this->name, $this->user_id, $this->status, $this->date, $this->id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return new Playlist($this->name, $this->user_id, $this->date, $this->type, $this->status, $this->id);
        }
        return FALSE;
    }

    public static function getById($id) {
        $q = "SELECT * FROM playlist WHERE id = ?";
        $params = [$id];
        $conn = new Connection;
        $playlists = False;
        $result = $conn::query($q, $params);
        foreach ($result as $row) {
            $playlists = new Playlist($row->name, $row->user_id, $row->date, $row->type, $row->status, $row->id);
        }
        return $playlists;
    }

    public static function getByUserId($id, $type) {
        $q = "SELECT * FROM playlist WHERE user_id = ? AND type = ?";
        $params = [$id, $type];
        $conn = new Connection;
        $playlists = False;
        $result = $conn::query($q, $params);
        foreach ($result as $row) {
            $playlists[] = new Playlist($row->name, $row->user_id, $row->date, $row->type, $row->status, $row->id);
        }
        return $playlists;
    }

    public static function getAll() {
        $q = "SELECT * FROM playlist";
        $conn = new Connection;
        $playlists = [];
        $result = $conn::query($q);
        if ($result) {
            foreach ($result as $row) {
                $playlists[] = new Playlist($row->name, $row->user_id, $row->date, $row->type, $row->status, $row->id);
            }return $playlists;
        }
    }

    public function getAllVideos() {
        $q = "SELECT * FROM videos WHERE playlist_id = ?";
        $params = [$this->id];
        $conn = new Connection;
        $videos = [];
        $result = $conn::query($q, $params);
        if ($result) {
            foreach ($result as $row) {
                $videos[] = new Video($row->user_id, $row->video_name, $row->date, $row->time, $row->description, $row->title, $row->channel_id, $row->playlist_id, $row->status, $row->id);
            }return $videos;
        }
    }
    
    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getUserId() {
        return $this->user_id;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getDate() {
        return $this->date;
    }

    public function getType() {
        return $this->type;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setUserId($user_id) {
        $this->user_id = $user_id;
        return $this;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    public function setDate($date) {
        $this->date = $date;
        return $this;
    }

    public function setType($type) {
        $this->type = $type;
        return $this;
    }


}

