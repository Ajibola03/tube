<?php

include_once "Connection.php";
include_once "Channel.php";
include_once "Video.php";

class Thumbnail extends Connection{
    private $id, $name, $video_id, $status, $conn;
    
    function __construct($name, $video_id, $status = 1, $id = null) {
        $this->id = $id;
        $this->name = $name;
        $this->video_id = $video_id;
        $this->status = $status;
        $this->conn = new Connection;
    }
    
    function add() {
        $q = "INSERT INTO thumbnails(name, video_id) VALUES (?,?)";
        $params = [$this->name, $this->video_id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return new Thumbnail($this->name, $this->video_id, $this->status, $this->id);    
        }
        return FALSE;
    }
    
    public static function getById($id){
        $q = "SELECT * FROM thumbnails WHERE id = ?";
        $params = [$id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        $thumbnail = False;
        foreach ($result as $value) {
            $thumbnail = new Thumbnail($value->name, $value->video_id, $value->status, $value->id);
        }
        return $thumbnail;
    }
    
    public static function getByVideoId($id){
        $q = "SELECT * FROM thumbnails WHERE video_id = ? AND status = 1";
        $params = [$id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        $thumbnail = False;
        foreach ($result as $value) {
            $thumbnail = new Thumbnail($value->name, $value->video_id, $value->status, $value->id);
        }
        return $thumbnail;
    }

        public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getVideoId() {
        return $this->video_id;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getConn() {
        return $this->conn;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setVideoId($video_id) {
        $this->video_id = $video_id;
        return $this;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    public function setConn($conn) {
        $this->conn = $conn;
        return $this;
    }


}