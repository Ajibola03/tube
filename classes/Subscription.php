<?php

include_once 'Connection.php';
include_once 'Video.php';

const STATUS_ACTIVE = 1;
const STATUS_INACTIVE = 0;

class Subscription extends Connection {

    private $id, $user_id, $channel_id, $status = 1, $conn;

    function __construct($user_id, $channel_id, $status = 1, $id = null) {
        $this->id = $id;
        $this->user_id = $user_id;
        $this->channel_id = $channel_id;
        $this->status = $status;
        $this->conn = new Connection;
    }

    public function add() {
        $q = "INSERT INTO subscriptions(user_id, channel_id, status) VALUES (?,?,?)";
        $params = [$this->user_id, $this->channel_id, $this->status];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return new Subscription($this->user_id, $this->channel_id, $this->status, $this->id);
        }
        return False;
    }

    function update() {
        $q = "UPDATE subscriptions SET user_id = ?, channel_id = ?, status = ? WHERE id = ?;";
        $params = [$this->user_id, $this->channel_id, $this->status, $this->id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return new Subscription($this->user_id, $this->channel_id, $this->status, $this->id);
        }
        return FALSE;
    }

    public static function getByUserId($user_id) {
        $q = "SELECT * FROM subscriptions WHERE user_id = ? AND status = ?";
        $Params = [$user_id, 1];
        $conn = new Connection;
        $result = $conn->query($q, $Params);
        $channel = False;
        foreach ($result as $row) {
            $channel = new Subscription($row->user_id, $row->channel_id, $row->status, $row->id);
        }if ($channel) {
            return $channel;
        } else {
            return False;
        }
    }

    public static function getVideos($channel_id) {
        $array1 = str_replace("[", "(", json_encode($channel_id));
        $array = str_replace("]", ")", $array1);
        $q = "SELECT * FROM videos WHERE channel_id IN" . $array . " AND status = ?";
        $params = [1];
        $conn = new Connection;
        $result = $conn->query($q, $params);
        $videos = [];
        if ($result) {
            foreach ($result as $row) {
                $videos[] = new Video($row->user_id, $row->video_name, $row->date, $row->time, $row->description, $row->title, $row->channel_id, $row->playlist_id, $row->status, $row->id);
            }return $videos;
        }
    }

    public static function getSub($user_id, $channel_id) {
        $q = "SELECT * FROM subscriptions WHERE user_id = ? AND channel_id = ? AND status = 1";
        $Params = [$user_id, $channel_id];
        $conn = new Connection;
        $result = $conn->query($q, $Params);
        $channel = False;
        foreach ($result as $row) {
            $channel = new Subscription($row->user_id, $row->channel_id, $row->status, $row->id);
        }if ($channel) {
            return $channel;
        } else {
            return False;
        }
    }

    public static function isSub($user_id, $channel_id) {
        $q = "SELECT * FROM subscriptions WHERE user_id = ? AND channel_id = ? AND status = 1";
        $Params = [$user_id, $channel_id];
        $conn = new Connection;
        $result = $conn->query($q, $Params);
        $channel = False;
        foreach ($result as $row) {
            $channel = new Subscription($row->user_id, $row->channel_id, $row->status, $row->id);
        }if ($channel) {
            return True;
        } else {
            return False;
        }
    }

    public static function getById($id) {
        $q = "SELECT * FROM subscriptions WHERE id = ?";
        $Params = [$id];
        $conn = new Connection;
        $result = $conn->query($q, $Params);
        $channel = False;
        foreach ($result as $row) {
            $channel = new Subscription($row->user_id, $row->channel_id, $row->status, $row->id);
        }
        return $channel;
    }

    public function getId() {
        return $this->id;
    }

    public function getUser_id() {
        return $this->user_id;
    }

    public function getChannel_id() {
        return $this->channel_id;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setUser_id($user_id) {
        $this->user_id = $user_id;
        return $this;
    }

    public function setChannel_id($channel_id) {
        $this->channel_id = $channel_id;
        return $this;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

}
