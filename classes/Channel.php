<?php

include_once "Connection.php";
include_once "User.php";

class Channel extends Connection {

    private $id, $logo, $user_id, $date, $status = 1, $name, $conn;

    function __construct($user_id, $logo, $date, $name, $status = 1, $id = null) {
        $this->id = $id;
        $this->logo = $logo;
        $this->user_id = $user_id;
        $this->status = $status;
        $this->name = $name;
        $this->date = $date;
        $this->conn = new Connection;
    }

    function add() {
        $q = "INSERT INTO channels(user_id,logo,name,status,date) VALUES (?,?,?,?,?)";
        $params = [$this->user_id, $this->logo, $this->name, $this->status, $this->date];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return new Channel($this->user_id, $this->logo, $this->date, $this->name, $this->status, $this->id);
        }
        return FALSE;
    }

    function update() {
        $q = "UPDATE users SET user_id = ?, logo = ?, name = ?, status = ?, date = ? WHERE id = ?;";
        $params = [$this->user_id, $this->logo, $this->name, $this->status, $this->date, $this->id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return new Channel($this->user_id, $this->logo, $this->date, $this->name, $this->status, $this->id);
        }
        return FALSE;
    }

    public static function getAll() {
        $q = "SELECT * FROM channels";
        $conn = new Connection;
        $result = $conn->query($q);
        $channels = [];
        foreach ($result as $row) {
            $channels[] = new Channel($row->user_id, $row->logo, $row->date, $row->name, $row->status, $row->id);
        }return $channels;
    }

    public static function getById($id) {
        $q = "SELECT * FROM channels WHERE id = ?";
        $Params = [$id];
        $conn = new Connection;
        $result = $conn->query($q, $Params);
        $channel = false;
        foreach ($result as $row) {
            $channel = new Channel($row->user_id, $row->logo, $row->date, $row->name, $row->status, $row->id);
        }return $channel;
    }

    public static function getByUserId($user_id) {
        $q = "SELECT * FROM channels WHERE user_id = ?";
        $Params = [$user_id];
        $conn = new Connection;
        $result = $conn->query($q, $Params);
        $channel = False;
        if ($result) {
            foreach ($result as $row) {
                $channel = new Channel($row->user_id, $row->logo, $row->date, $row->name, $row->status, $row->id);
            }return $channel;
        }
    }

    public function getLikes() {
        $q = "SELECT COUNT(channel_id) AS likes FROM likes WHERE channel_id = ?";
        $params = [$this->id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return $result[0]->likes;
        }
        return false;
    }

    public function __toString() {
        return $this->name;
    }

    public function getId() {
        return $this->id;
    }

    public function getUserId() {
        return $this->lastname;
    }

    public function getLogo() {
        return $this->logo;
    }

    public function getDate() {
        return $this->date;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getName() {
        return $this->name;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setUserId($id) {
        $this->user_id = $id;
        return $this;
    }

    public function setLogo($logo) {
        $this->logo = $logo;
        return $this;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setDate($date) {
        $this->date = $date;
        return $this;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

}
