<?php

include_once "Connection.php";
include_once "Channel.php";
include_once "User.php";
include_once "Video.php";

class Comment extends Connection {

    private $id, $video_id, $user_id, $comment, $type, $at, $time, $date, $status = 1, $conn;

    function __construct($video_id, $user_id, $comment, $type, $at, $time, $date, $status = 1, $id = null) {
        $this->id = $id;
        $this->video_id = $video_id;
        $this->user_id = $user_id;
        $this->comment = $comment;
        $this->type = $type;
        $this->at = $at;
        $this->time = $time;
        $this->date = $date;
        $this->status = $status;
        $this->conn = new Connection;
    }

    function add() {
        $q = "INSERT INTO comments(video_id, user_id, comment, type, at, time, date, status) VALUES (?,?,?,?,?,?,?,?)";
        $params = [$this->video_id, $this->user_id, $this->comment, $this->type, $this->at, $this->time, $this->date, $this->status];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return new Comment($this->video_id, $this->user_id, $this->comment, $this->type, $this->at, $this->time, $this->date, $this->status, $this->id);
        }
        return FALSE;
    }

    function update() {
        $q = "UPDATE comments SET video_id = ?, user_id = ?, comment = ?, type = ?, at = ?, time = ?, date = ?, status = ? WHERE id = ?;";
        $params = [$this->video_id, $this->user_id, $this->comment, $this->type, $this->at, $this->time, $this->date, $this->status, $this->id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return new Comment($this->video_id, $this->user_id, $this->comment, $this->time, $this->date, $this->status, $this->id);
        }
        return FALSE;
    }

    public static function getAll() {
        $q = "SELECT * FROM comments";
        $conn = new Connection;
        $result = $conn::query($q);
        $comments = [];
        foreach ($result as $row) {
            $comments[] = new Comment($row->video_id, $row->user_id, $row->comment, $row->type, $row->at, $row->time, $row->date, $row->status, $row->id);
        }return $comments;
    }
    
    public static function getAllComments() {
        $q = "SELECT * FROM comments WHERE type = 1";
        $conn = new Connection;
        $result = $conn::query($q);
        $comments = [];
        foreach ($result as $row) {
            $comments[] = new Comment($row->video_id, $row->user_id, $row->comment, $row->type, $row->at, $row->time, $row->date, $row->status, $row->id);
        }return $comments;
    }

    public static function getAllByVideoId($id) {
        $q = "SELECT * FROM comments WHERE type = 1 AND video_id = ?";
        $params = [$id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        $comments = [];
        foreach ($result as $row) {
            $comments[] = new Comment($row->video_id, $row->user_id, $row->comment, $row->type, $row->at, $row->time, $row->date, $row->status, $row->id);
        }return $comments;
    }
    
    public function getAllReplies() {
        $q = "SELECT * FROM comments WHERE type = 2 AND at = ?";
        $params = [$this->id];
        $result = $this->conn::query($q, $params);
        $comments = [];
        foreach ($result as $row) {
            $comments[] = new Comment($row->video_id, $row->user_id, $row->comment, $row->type, $row->at, $row->time, $row->date, $row->status, $row->id);
        }return $comments;
    }

    public static function getAllRel($video_id) {
        $q = "SELECT * FROM comments WHERE video_id = ?";
        $params = [$video_id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        $comments = [];
        foreach ($result as $row) {
            $comments[] = new Comment($row->video_id, $row->user_id, $row->comment, $row->type, $row->at, $row->time, $row->date, $row->status, $row->id);
        }return $comments;
    }

    public static function getById($id) {
        $q = "SELECT * FROM comments WHERE id = ?";
        $params = [$id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        $comment = False;
        if ($result) {
            foreach ($result as $row) {
                $comment = new Comment($row->video_id, $row->user_id, $row->comment, $row->type, $row->at, $row->time, $row->date, $row->status, $row->id);
            }
        }
        return $comment;
    }
    
    public function getUser() {
        $user = User::getById($this->user_id);
        return $user;
    }

    public function getAtName() {
        $q = "SELECT * FROM comments WHERE id = ?";
        $params = [$this->at];
        $results = $this->conn::query($q, $params);
        if ($results) {
            $result = $results[0];
            $comment = new Comment($result->video_id, $result->user_id, $result->comment, $result->type, $result->at, $result->time, $result->date, $result->status, $result->id);
            $user = User::getById($comment->user_id);
            return $user;
        }
        return "replyerror";
    }
    
//    public function getLikes() {
//        $q = "SELECT COUNT(user_id) AS likes FROM likes WHERE user_id = ?";
//        $params = [$this->id];
//        $result = $this->conn::query($q, $params);
//        if ($result) {
//            return $result[0]->likes;
//        }
//        return False;
//    }

    public function __toString() {
        return $this->comment;
    }
    
    public function getId() {
        return $this->id;
    }

    public function getVideoId() {
        return $this->video_id;
    }

    public function getUserId() {
        return $this->user_id;
    }

    public function getComment() {
        return $this->comment;
    }

    public function getType() {
        return $this->type;
    }

    public function getStringType() {
        $type = $this->type;
        if ($type == 1){
            $string = "comment";
        }else{
            $string = "reply";
        }
    }

    public function getAt() {
        return $this->at;
    }

    public function getProfilePic() {
        $user = User::getById($this->user_id);
        $logo = $user->getLogo();
        return $logo;
    }
    
    public function getTime() {
        return $this->time;
    }
    
    public function getTimeDiff(){
        $result = time_diff($this->date . " " . $this->time);
        return $result;
    }

    public function getDate() {
        return $this->date;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getConn() {
        return $this->conn;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setVideoId($video_id) {
        $this->video_id = $video_id;
        return $this;
    }

    public function setUserId($user_id) {
        $this->user_id = $user_id;
        return $this;
    }

    public function setComment($comment) {
        $this->comment = $comment;
        return $this;
    }

    public function setType($type) {
        $this->type = $type;
        return $this;
    }

    public function setAt($at) {
        $this->at = $at;
        return $this;
    }

    public function setTime($time) {
        $this->time = $time;
        return $this;
    }

    public function setDate($date) {
        $this->date = $date;
        return $this;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    public function setConn($conn) {
        $this->conn = $conn;
        return $this;
    }

}
