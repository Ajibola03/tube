<?php
if(!isset($_SESSION)){
session_start();
}
$URL = $_SERVER['PHP_SELF'];
$current_pages = explode('/', $URL);
$current_page = strtolower(end($current_pages));
// $admin = strtolower($current_pages[2]);
// if ($current_page !== "index.php") {
//     if ($admin !== "admin") {
//         include_once '../config.php';
//         include_once '../helper.php';
//     }else{
//         include_once '../../config.php';
//         include_once '../../helper.php';
//     }
// }

class Connection {

    const servername = "localhost", username = "root", password = "", dbname = 'tube';

    private static $conn;

    public static function connect() {
        try {
            self::$conn = new PDO("mysql:host=" . self::servername . ";dbname=" . self::dbname, self::username, self::password);
            // set the PDO error mode to exception
            self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return self::$conn;
        } catch (PDOException $e) {
            return false;
        }
    }

    public static function query($sql, $params = []) {
        $conn = self::connect();
        $stm = $conn->prepare($sql);
        $i = 1;
        foreach ($params as $param) {
            $stm->bindValue($i, $param);
            $i++;
        }
        try {
            $result = $stm->execute();
            if (preg_match('/^update/i', $sql) || preg_match('/^INSERT/i', $sql))
                return $result;
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (PDOException $e) {
            return false;
        }
    }

}
