<?php

include_once "Connection.php";
include_once "Channel.php";
include_once "Video.php";
include_once "Channel.php";

class View extends Connection {

    private $id, $video_id, $user_id, $ad, $channel_id, $poster_id, $date, $ref_id, $status, $conn;

    function __construct($video_id, $user_id, $ad, $channel_id, $poster_id, $date, $ref_id, $status = 1, $id = null) {
        $this->id = $id;
        $this->video_id = $video_id;
        $this->user_id = $user_id;
        $this->ad = $ad;
        $this->ref_id = $ref_id;
        $this->channel_id = $channel_id;
        $this->poster_id = $poster_id;
        $this->date = $date;
        $this->status = $status;
        $this->conn = new Connection;
    }

    function add() {
        $q = "INSERT INTO views(video_id, user_id, ad, channel_id, poster_id, date, status, ref_id) VALUES (?,?,?,?,?,?,?,?)";
        $params = [$this->video_id, $this->user_id, $this->ad, $this->channel_id, $this->poster_id, $this->date, $this->status, $this->ref_id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return new View($this->video_id, $this->user_id, $this->ad, $this->channel_id, $this->poster_id, $this->date, $this->ref_id, $this->status, $this->id);
        }
        return FALSE;
    }

    function update() {
        $q = "UPDATE views SET video_id = ?, user_id = ?, ad = ?, channel_id = ?, poster_id = ?, date = ?, status = ?, ref_id = ? WHERE id = ?;";
        $params = [$this->video_id, $this->user_id, $this->ad, $this->channel_id, $this->poster_id, $this->date, $this->status, $this->ref_id, $this->id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return new View($this->video_id, $this->user_id, $this->ad, $this->channel_id, $this->poster_id, $this->date, $this->ref_id, $this->status, $this->id);
        }
        return FALSE;
    }

    public static function getAll() {
        $q = "SELECT * FROM views";
        $result = query($q);
        $likes = [];
        foreach ($result as $row) {
            $likes[] = new View($row->video_id, $row->user_id, $row->ad, $row->channel_id, $row->poster_id, $this->date, $this->ref_id, $this->status, $row->id);
        }return $likes;
    }

    public static function getById($id) {
        $q = "SELECT * FROM views WHERE id = ?";
        $params = [$id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        foreach ($result as $row) {
            $views = new View($row->video_id, $row->user_id, $row->ad, $row->channel_id, $row->poster_id, $row->date, $row->ref_id, $row->status, $row->id);
        }
        return $views;
    }

    public static function getByUserId($id) {
        $q = "SELECT * FROM views WHERE poster_id = ?";
        $params = [$id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        foreach ($result as $row) {
            $views[] = new View($row->video_id, $row->user_id, $row->ad, $row->channel_id, $row->poster_id, $row->date, $row->ref_id, $row->status, $row->id);
        }
        return $views;
    }

    public static function getHistory($user_id) {
        $q = "SELECT video_id FROM views WHERE user_id = ? AND status = 1";
        $params = [$user_id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        $videos = [];
        foreach ($result as $row) {
            $videos[] = $row->video_id;
        }
        return $videos;
    }

    public static function checkViewed($video_id,$user_id) {
        $q = "SELECT * FROM views WHERE video_id = ? AND user_id = ?";
        $params = [$video_id,$user_id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        $likes = False;
        if ($result) {
            $likes = True;
        }
        return $likes;
    }
    
    public static function moveAll($user_id, $channel_id) {
        $videos = View::getByUserId($user_id);
        foreach ($videos as $video) {
            $video->setChannelId($channel_id);
            $video->update();
            $_SESSION["move_all"] = False;
        }flash("success", "Videos successfully moved");
    }

    public function getChannel() {
        $q = "SELECT * FROM channels WHERE user_id = ?";
        $params = [$this->poster_id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            $channel = Channel::getById($result);
            return $channel;
        }
        return false;
    }

    public function getId() {
        return $this->id;
    }

    public function getVideoId() {
        return $this->video_id;
    }

    public function getUserId() {
        return $this->user_id;
    }

    public function getAd() {
        return $this->ad;
    }

    public function getChannelId() {
        return $this->channel_id;
    }

    public function getPosterId() {
        return $this->poster_id;
    }

    public function getDate() {
        return $this->date;
    }

    public function getRefId() {
        return $this->ref_id;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setVideoId($video_id) {
        $this->video_id = $video_id;
        return $this;
    }

    public function setUserId($user_id) {
        $this->user_id = $user_id;
        return $this;
    }

    public function setAd($ad) {
        $this->ad = $ad;
        return $this;
    }

    public function setChannelId($channel_id) {
        $this->channel_id = $channel_id;
        return $this;
    }

    public function setPosterId($poster_id) {
        $this->poster_id = $poster_id;
        return $this;
    }

    public function setRefId($id) {
        $this->ref_id = $id;
        return $this;
    }

    public function setdate($date) {
        $this->date = $date;
        return $this;
    }

    public function setStatus($status) {
        $this->date = $status;
        return $this;
    }
}