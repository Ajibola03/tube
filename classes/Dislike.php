<?php

include_once "Connection.php";
include_once "Channel.php";
include_once "Video.php";
include_once "Channel.php";

class Dislike extends Connection {

    private $id, $video_id, $user_id, $channel_id, $poster_id, $conn;

    function __construct($video_id, $user_id, $poster_id, $channel_id, $id = null) {
        $this->id = $id;
        $this->video_id = $video_id;
        $this->user_id = $user_id;
        $this->channel_id = $channel_id;
        $this->poster_id = $poster_id;
        $this->conn = new Connection;
    }

    function add() {
        $q = "INSERT INTO dislikes(video_id, user_id, poster_id, channel_id) VALUES (?,?,?,?)";
        $params = [$this->video_id, $this->user_id, $this->poster_id, $this->channel_id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return new Dislike($this->video_id, $this->user_id, $this->poster_id, $this->channel_id, $this->id);
        }
        return FALSE;
    }

    function upadate() {
        $q = "UPDATE dislikes SET video_id = ?, user_id = ?, poster_id = ?, channel_id = ?, WHERE id = ?;";
        $params = [$this->video_id, $this->user_id, $this->poster_id, $this->channel_id, $this->id];
        $result = query($q, $params);
        if ($result) {
            return new Dislike($this->video_id, $this->user_id, $this->poster_id, $this->channel_id, $this->id);
        }
        return FALSE;
    }

    public static function getAll() {
        $q = "SELECT * FROM dislikes";
        $result = query($q);
        $users = [];
        foreach ($result as $row) {
            $users[] = new Dislike($row->video_id, $row->user_id, $row->poster_id, $row->channel_id, $row->result);
        }return $users;
    }

    public static function getById($id) {
        $q = "SELECT * FROM dislikes WHERE id = ?";
        $params = [$id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        foreach ($result as $row) {
            $like = new Dislike($row->video_id, $row->user_id, $row->poster_id, $row->channel_id, $row->id);
        }
        return $like;
    }

    public static function getByUserId($user_id,$video_id) {
        $q = "SELECT * FROM dislikes WHERE user_id = ? AND video_id = ?";
        $params = [$user_id, $video_id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        if ($result) {
            return True;
        }
        return false;
    }
    
    public function getId() {
        return $this->id;
    }

    public function getVideoId() {
        return $this->video_id;
    }

    public function getUserId() {
        return $this->user_id;
    }

    public function getChannelId() {
        return $this->channel_id;
    }

    public function getPosterId() {
        return $this->poster_id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setVideoId($video_id) {
        $this->video_id = $video_id;
        return $this;
    }

    public function setUserId($user_id) {
        $this->user_id = $user_id;
        return $this;
    }

    public function setChannelId($channel_id) {
        $this->channel_id = $channel_id;
        return $this;
    }

    public function setPosterId($poster_id) {
        $this->poster_id = $poster_id;
        return $this;
    }
}