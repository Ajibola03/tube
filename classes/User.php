<?php

include_once "Connection.php";
include_once "Channel.php";
include_once "Video.php";

class User extends Connection {

    private $id, $lastname, $firstname, $email, $password, $logo, $date, $country, $status = 1, $conn;

    function __construct($lastname, $firstname, $email, $password, $logo, $date, $country, $status = 1, $id = null) {
        $this->id = $id;
        $this->lastname = $lastname;
        $this->firstname = $firstname;
        $this->email = $email;
        $this->logo = $logo;
        $this->status = $status;
        $this->date = $date;
        $this->country = $country;
        $this->password = $password;
        $this->conn = new Connection;
    }

    function add() {
        $q = "INSERT INTO users(lastname,firstname,email,password,logo,status,date,country) VALUES (?,?,?,?,?,?,?,?)";
        $params = [$this->lastname, $this->firstname, $this->email, $this->password, $this->logo, $this->status, $this->date, $this->country];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return new User($this->lastname, $this->firstname, $this->email, $this->password, $this->logo, $this->date, $this->country, $this->status, $this->id);
        }
        return FALSE;
    }

    function update() {
        $q = "UPDATE users SET lastname = ?, firstname = ?, email = ?, password = ?, logo = ?, status = ?, date = ?, country = ? WHERE id = ?;";
        $params = [$this->lastname, $this->firstname, $this->email, $this->password, $this->logo, $this->status, $this->date, $this->country, $this->id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return new User($this->lastname, $this->firstname, $this->email, $this->password, $this->logo, $this->date, $this->country, $this->status, $this->id);
        }
        return FALSE;
    }

    public static function getAll() {
        $q = "SELECT * FROM users";
        $conn = new Connection;
        $result = $conn::query($q);
        $users = [];
        foreach ($result as $row) {
            $users[] = new User($row->lastname, $row->firstname, $row->email, $row->password, $row->logo, $row->date, $row->country, $row->status, $row->id);
        }return $users;
    }

    public static function getLastId(){
        $q = "SELECT MAX(id) AS last_id FROM users";
        $conn = new Connection;
        $result = $conn::query($q);
        if ($result){
            return $result[0]->last_id;
        }
    }

    public static function getNew() {
        $q = "SELECT * FROM users WHERE date = ?";
        $params = [date("Y/m/d")];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        $users = [];
        foreach ($result as $row) {
            $users[] = new User($row->lastname, $row->firstname, $row->email, $row->password, $row->logo, $row->date, $row->country, $row->status, $row->id);
        }return $users;
    }

    public static function getById($id) {
        $q = "SELECT * FROM users WHERE id = ?";
        $params = [$id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        $user = False;
        if ($result) {
            foreach ($result as $row) {
                $user = new User($row->lastname, $row->firstname, $row->email, $row->password, $row->logo, $row->date, $row->country, $row->status, $row->id);
            }
        }
        return $user;
    }

    public static function getByUserId($user_id) {
        $q = "SELECT * FROM users WHERE user_id = ?";
        $params = [$user_id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        $user = False;
        foreach ($result as $row) {
            $user = new User($row->lastname, $row->firstname, $row->email, $row->password, $row->logo, $row->date, $row->country, $row->status, $row->id);
        }
        return $user;
    }

    public static function getByEmail($email) {
        $q = "SELECT * FROM users WHERE email = ?";
        $params = [$email];
        $conn = new Connection;
        $user = False;
        $result = $conn::query($q, $params);
        foreach ($result as $row) {
            $user = new User($row->lastname, $row->firstname, $row->email, $row->password, $row->logo, $row->date, $row->country, $row->status, $row->id);
        }
        return $user;
    }

    public function getChannel() {
        $q = "SELECT * FROM channels WHERE user_id = ?";
        $params = [$this->id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            $channel = Channel::getById($result[0]->id);
            return $channel;
        }
        return False;
    }

    public function getChannelName() {
        if ($this->getChannel()) {
            $q = "SELECT name FROM channels WHERE id = ?";
            $params = [$this->getChannel()->getId()];
            $result = $this->conn::query($q, $params);
            if ($result) {
                return $result[0]->name;
            }
            return $this . " ";
        }
        return $this." ";
    }

    public function getMyMoney() {
        $q = "SELECT * FROM money_users WHERE user_id = ?";
        $params = [$this->id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            $money = Money::getById($result);
            return $money;
        }
        return False;
    }

    public function getNoOfVideos()
    {
        $q = "SELECT COUNT(*) AS likes FROM videos WHERE user_id = ?";
        $params = [$this->id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return $result[0]->likes;
        }
        return 0;
    }

    public function getSubscribedChannels() {
        $q = "SELECT channel_id FROM `subscriptions` WHERE user_id = ? AND status = 1";
        $params = [$this->id];
        $result = $this->conn::query($q, $params);
        $channels = [];
        if ($result) {
            foreach ($result as $row) {
                $channels[] = $row->channel_id;
                return $channels;
            }
        }
        return False;
    }

    public function getLikes() {
        $q = "SELECT COUNT(user_id) AS likes FROM likes WHERE user_id = ?";
        $params = [$this->id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return $result[0]->likes;
        }
        return False;
    }

    public function __toString() {
        return $this->lastname . " " . $this->firstname;
    }

    public function getId() {
        return $this->id;
    }

    public function getLastname() {
        return $this->lastname;
    }

    public function getFirstname() {
        return $this->firstname;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getLogo() {
        return $this->logo;
    }

    public function getDate() {
        return $this->date;
    }

    public function getCountry() {
        return $this->country;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getStatusString() {
        switch($this->status){
            case 0:
                $str = "Inactive";
                break;
            case 1:
                $str = "Active";
                break;
            case 2:
                $str = "Banned";
                break;
            default :
                $str = "An error occured";
                break;
        }return $str;
    }

    public function getStatusBadge() {
        switch($this->status){
            case 0:
                $str = "<div class='badge badge-sm badge-warning'>Inactive</div>";
                break;
            case 1:
                $str = "<div class='badge badge-sm badge-success'>Active</div>";
                break;
            case 2:
                $str = "<div class='badge badge-sm badge-danger'>Banned</div>";
                break;
            default :
                $str = "An error occured";
                break;
        }return $str;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setLastname($lastname) {
        $this->lastname = $lastname;
        return $this;
    }

    public function setFirstname($firstname) {
        $this->firstname = $firstname;
        return $this;
    }

    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }

    public function setPassword($password) {
        $this->password = $password;
        return $this;
    }

    public function setLogo($logo) {
        $this->logo = $logo;
        return $this;
    }

    public function setDate($date) {
        $this->date = $date;
        return $this;
    }

    public function setCountry($country) {
        $this->country = $country;
        return $this;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

}
