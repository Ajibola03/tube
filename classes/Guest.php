<?php

include_once 'Connection.php';

class Guest {

    private $id, $status = 1, $date, $conn;

    function __construct($date, $status = 1, $id = null) {
        $this->date = $date;
        $this->status = $status;
        $this->id = $id;
        $this->conn = new Connection;
    }

    function add() {
        $q = "INSERT INTO guests(status,date) VALUES (?,?)";
        $params = [$this->status, $this->date];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return new Guest($this->date);
        }
        return False;
    }

    public function getId() {
        return $this->id;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getDate() {
        return $this->date;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    public function setDate($date) {
        $this->date = $date;
        return $this;
    }

}
