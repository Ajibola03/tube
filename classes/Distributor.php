<?php

include_once '../config.php';
include_once 'Connection.php';

class Distributor extends Connection{
    
    private $user_id, $date, $time, $status = 1, $id = null, $conn;
    
    function __construct($user_id, $date, $time, $status = 1, $id = null) {
        $this->user_id = $user_id;
        $this->date = $date;
        $this->time = $time;
        $this->status = $status;
        $this->id = $id;
        $this->conn = new Connection;
    }
    
    public function add(){
        $q = "INSERT INTO distributor(user_id, date, time, status) VALUES(?, ?, ?, ?)";
        $params = [$this->user_id, $this->date, $this->time, $this->status];
        $result = $this->conn::query($q, $params);
        if($result){
            return new Distributor($this->user_id, $this->date, $this->time, $this->status, $this->id);
        }
        return False;
    }
    
    public function update() {
        $q = "UPDATE distributor SET user_id = ?, date = ?, time = ?, status = ? WHERE id = ?;";
        $params = [$this->user_id, $this->date, $this->time, $this->status, $this->id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return new Distributor($this->user_id, $this->date, $this->time, $this->status, $this->id);
        }
        return FALSE;
    }

    public static function getById($id) {
        $q = "SELECT * FROM distributor WHERE id = ?";
        $params = [$id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        foreach ($result as $row) {
            $money = new Distributor($this->user_id, $this->date, $this->time, $this->status, $this->id);
        }
        return $money;
    }

    public function getUserId() {
        return $this->user_id;
    }

    public function getDate() {
        return $this->date;
    }

    public function getTime() {
        return $this->time;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getId() {
        return $this->id;
    }

    public function getConn() {
        return $this->conn;
    }

    public function setUser_id($user_id) {
        $this->user_id = $user_id;
        return $this;
    }

    public function setDate($date) {
        $this->date = $date;
        return $this;
    }

    public function setTime($time) {
        $this->time = $time;
        return $this;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setConn($conn) {
        $this->conn = $conn;
        return $this;
    }


}

