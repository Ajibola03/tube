<?php

include_once "Connection.php";
include_once "Channel.php";
include_once "Comment.php";
include_once 'Thumbnail.php';
include_once "View.php";

class Video extends Connection {

    private $id, $user_id, $channel_id, $video_name, $date, $time, $status = 1, $description, $title, $playlist_id, $conn;

    function __construct($user_id, $video_name, $date, $time, $description, $title, $channel_id = null, $playlist_id = null, $status = 1, $id = null) {
        $this->id = $id;
        $this->user_id = $user_id;
        $this->playlist_id = $playlist_id;
        $this->channel_id = $channel_id;
        $this->video_name = $video_name;
        $this->date = $date;
        $this->time = $time;
        $this->status = $status;
        $this->description = $description;
        $this->title = $title;
        $this->conn = new Connection;
    }

    function add() {
        $q = "INSERT INTO videos(user_id, channel_id, video_name, status, description, time, date, title, playlist_id) VALUES (?,?,?,?,?,?,?,?,?)";
        $params = [$this->user_id, $this->channel_id, $this->video_name, $this->status, $this->description, $this->time, $this->date, $this->title, $this->playlist_id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return new Video($this->user_id, $this->video_name, $this->date, $this->time, $this->description, $this->title, $this->channel_id, $this->playlist_id, $this->status, $this->id);
        }
        return FALSE;
    }

    function update() {
        $q = "UPDATE videos SET user_id = ?, channel_id = ?, video_name = ?, date = ?, time = ?,  status = ?, description = ?, title = ?, playlist_id = ? WHERE id = ?";
        $params = [$this->user_id, $this->channel_id, $this->video_name, $this->date, $this->time, $this->status, $this->description, $this->title, $this->playlist_id, $this->id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return new Video($this->user_id, $this->video_name, $this->date, $this->time, $this->description, $this->title, $this->channel_id, $this->playlist_id, $this->status, $this->id);
        }
        return FALSE;
    }

    public static function getAll() {
        $q = "SELECT * FROM videos";
        $conn = new Connection;
        $videos = [];
        $result = $conn::query($q);
        if ($result) {
            foreach ($result as $row) {
                $videos[] = new Video($row->user_id, $row->video_name, $row->date, $row->time, $row->description, $row->title, $row->channel_id, $row->playlist_id, $row->status, $row->id);
            }return $videos;
        }return $videos;
    }

    public static function getLastId(){
        $q = "SELECT MAX(id) AS last_id FROM videos";
        $conn = new Connection;
        $result = $conn::query($q);
        if ($result){
            return $result[0]->last_id;
        }
    }

    public static function getTrending() {
        $q = "SELECT video_id, COUNT(video_id) AS views FROM views GROUP BY video_id ORDER BY views DESC LIMIT 10";
        $conn = new Connection;
        $videos = [];
        $result = $conn::query($q);
        $fresh = false;
        foreach ($result as $video) {
            $fresh[] = $video->video_id;
        }
        if ($fresh) {
            foreach ($fresh as $row) {
                $videos[] = Video::getById($row);
            }
            return $videos;
        }
        return $videos;
    }

    public static function getAllAvl() {
        $q = "SELECT * FROM videos WHERE status = 1";
        $conn = new Connection;
        $result = $conn::query($q);
        $videos = [];
        foreach ($result as $row) {
            $videos[] = new Video($row->user_id, $row->video_name, $row->date, $row->time, $row->description, $row->title, $row->channel_id, $row->playlist_id, $row->status, $row->id);
        }return $videos;
    }

    public static function getById($id) {
        $q = "SELECT * FROM videos WHERE id = ?";
        $params = [$id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        $videos = null;
        foreach ($result as $row) {
            $videos = new Video($row->user_id, $row->video_name, $row->date, $row->time, $row->description, $row->title, $row->channel_id, $row->playlist_id, $row->status, $row->id);
        }return $videos;
    }

    public static function getByUserId($user_id) {
        $q = "SELECT * FROM videos WHERE user_id = ?";
        $params = [$user_id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        $videos = [];
        foreach ($result as $row) {
            $videos[] = new Video($row->user_id, $row->video_name, $row->date, $row->time, $row->description, $row->title, $row->channel_id, $row->playlist_id, $row->status, $row->id);
        }return $videos;
    }

    public static function getByChannelId($id) {
        $q = "SELECT * FROM videos WHERE channel_id = ?";
        $params = [$id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        $videos = False;
        foreach ($result as $row) {
            $videos[] = new Video($row->user_id, $row->video_name, $row->date, $row->time, $row->description, $row->title, $row->channel_id, $row->playlist_id, $row->status, $row->id);
        }return $videos;
    }
    
    public function getComments() {
        $result = Comment::getAllByVideoId($this->id);
        return $result;
    }

    public static function moveAll($user_id, $channel_id) {
        $videos = Video::getByUserId($user_id);
        foreach ($videos as $video) {
            $video->setChannelId($channel_id);
            $video->update();
            $_SESSION["move_all"] = False;
        }flash("success", "Videos successfully moved");
    }

    public function getRel() {
        $q = "SELECT * FROM videos WHERE channel_id = ?";
        $params = [$this->id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        $videos = [];
        foreach ($result as $row) {
            $videos[] = new Video($row->user_id, $row->video_name, $row->date, $row->time, $row->description, $row->title, $row->channel_id, $row->playlist_id, $row->status, $row->id);
        }return $videos;
    }

    public static function search($value) {
        $title = str_ireplace(" ", '%', $value);
        $q = "SELECT * FROM videos WHERE title LIKE '%" . $title . "%' AND status = 1 ORDER BY title";
        $conn = new Connection;
        $result = $conn::query($q);
        $videos = [];
        foreach ($result as $row) {
            $videos[] = new Video($row->user_id, $row->video_name, $row->date, $row->time, $row->description, $row->title, $row->channel_id, $row->playlist_id, $row->status, $row->id);
        }return $videos;
    }

    public function getChannel() {
        $q = "SELECT * FROM channels WHERE user_id = ?";
        $params = [$this->user_id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return True;
        }
        return False;
    }

    public function getThumbnail() {
        $q = "SELECT name FROM thumbnails WHERE video_id = ?";
        $params = [$this->id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return $result[0]->name;
        }
        return "desktop1.png";
    }

    public function getThumbnailPath() {
        $q = "SELECT name FROM thumbnails WHERE video_id = ?";
        $params = [$this->id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return THUMBS_NETWORK_PATH . $result[0]->name;
        }
        return False;
    }
    
    public static function getIdFromName($name) {
        $q = "SELECT id FROM videos WHERE video_name = ?";
        $params = [$name];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        if ($result) {
            return $result[0]->id;
        }
        return False;
    }

    public function getChannelName() {
        $q = "SELECT name FROM channels WHERE id = ?";
        $params = [$this->channel_id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return $result[0]->name;
        }
        return User::getById($this->user_id) . " ";
    }

    public function getLikes() {
        $q = "SELECT COUNT(user_id) AS likes FROM likes WHERE video_id = ?";
        $params = [$this->id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return $result[0]->likes . " ";
        }
        return false;
    }

    public static function getLiked($id) {
        $q = "SELECT * FROM likes WHERE user_id = ?";
        $params = [$id];
        $conn = new Connection;
        $result = $conn::query($q, $params);
        $videos = [];
        if ($result) {
            foreach ($result as $row) {
                $videos[] = Video::getById($row->video_id);
            }return $videos;
        }
    }

    public function getDislikes() {
        $q = "SELECT COUNT(user_id) AS dislikes FROM dislikes WHERE video_id = ?";
        $params = [$this->id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return $result[0]->dislikes . " ";
        }
        return false;
    }
    
    public function getPath() {
        $path = UPLOAD_VIDEO_PATH.$this->video_name;
        return $path;
    }

    public function getViews() {
        $q = "SELECT COUNT(video_id) AS views FROM views WHERE video_id = ?";
        $params = [$this->id];
        $result = $this->conn::query($q, $params);
        if ($result) {
            return $result[0]->views . " ";
        }
        return false;
    }

    public function __toString() {
        return $this->video_name;
    }

    public function getId() {
        return $this->id;
    }

    public function getUserId() {
        return $this->user_id;
    }

    public function getUser() {
        return User::getById($this->user_id);
    }

    public function getLogo() {
        return User::getById($this->user_id)->getLogo();
    }

    public function getLogoPath() {
        return PROFILE_NETWORK_PATH.User::getById($this->user_id)->getLogo();
    }

    public function getChannelId() {
        return $this->channel_id;
    }

    public function getPlaylistId() {
        return $this->playlist_id;
    }

    public function getVideoname() {
        return $this->video_name;
    }

    public function getDate() {
        return $this->date;
    }

    public function getTime() {
        return $this->time;
    }

    public function getElpsTime() {
        $result = time_diff($this->date . " " . $this->time);
        return $result;
    }

    public function getStrTime() {
        return $this->time;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getStatusString() {
        switch($this->status){
            case 0:
                $str = "Inactive";
                break;
            case 1:
                $str = "Active";
                break;
            case 2:
                $str = "Banned";
                break;
            default :
                $str = "An error occured";
                break;
        }return $str;
    }

    public function getStatusBadge() {
        switch($this->status){
            case 0:
                $str = "<div class='badge badge-sm badge-warning'>Inactive</div>";
                break;
            case 1:
                $str = "<div class='badge badge-sm badge-success'>Active</div>";
                break;
            case 2:
                $str = "<div class='badge badge-sm badge-danger'>Banned</div>";
                break;
            default :
                $str = "An error occured";
                break;
        }return $str;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getFriendlyDescription() {
        $q = "SELECT MID(description,1,70) AS description FROM videos WHERE id = ?";
        $params = [$this->id];
        $result = $this->conn::query($q, $params);
        if (strlen($this->description) > 70) {
            return $result[0]->description . "...";
        } else {
            return $this->description;
        }
    }

    public function getTitle() {
        return $this->title;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setUserId($user_id) {
        $this->user_id = $user_id;
        return $this;
    }

    public function setChannelId($channel_id) {
        $this->channel_id = $channel_id;
        return $this;
    }

    public function setPlaylistId($playlist_id) {
        $this->channel_id = $playlist_id;
        return $this;
    }

    public function setVideoname($video_name) {
        $this->video_name = $video_name;
        return $this;
    }

    public function setDate($date) {
        $this->date = $date;
        return $this;
    }

    public function setTime($time) {
        $this->time = $time;
        return $this;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }

}
