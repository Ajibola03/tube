CREATE TABLE IF NOT EXISTS `users` (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    lastname varchar(255),
    firstname varchar(255),
    email varchar(255) UNIQUE NOT NULL,
    password varchar(255) NOT NULL,
    logo varchar(255),
    `status` tinyint(1) COMMENT "0=inactive,1=active,2=banned" DEFAULT 1,
    `date` date NOT NULL,
    country varchar(225) NOT NULL
);


CREATE TABLE IF NOT EXISTS guests (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `status` tinyint(1) COMMENT "0=inactive,1=active" DEFAULT 1,
    `date` date NOT NULL
);

CREATE TABLE IF NOT EXISTS channels (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id int UNIQUE NOT NULL,
    logo varchar(255),
    `status` tinyint(1) COMMENT "0=deleted,1=avaailable,2=bannned" DEFAULT 1,
    `date` date NOT NULL,
    `name` varchar(225) NOT NULL,
    FOREIGN KEY fk_ch_us_id (user_id)  REFERENCES users(id)
);

CREATE TABLE IF NOT EXISTS playlist (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `name` varchar(225) NOT NULL,
    user_id int NOT NULL,
    `status` tinyint(1) COMMENT "0=deleted,1=avaailable" DEFAULT 1,
    `date` date NOT NULL,
    `type` tinyint(1) COMMENT "1=user,2=channel" DEFAULT 1,
    FOREIGN KEY fk_py_us_id (user_id)  REFERENCES users(id)
);

CREATE TABLE IF NOT EXISTS videos (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id int NOT NULL,
    channel_id  int,
    video_name varchar(255) NOT NULL,
    description varchar(255),
    `status` tinyint(1) COMMENT "0=deleted,1=avaailable,2=bannned" DEFAULT 1,
    `time` time NOT NULL,
    `date` date NOT NULL,
    `title` varchar(225) NOT NULL,
    playlist_id int,
    FOREIGN KEY fk_vd_us_id (user_id) REFERENCES users(id),
    FOREIGN KEY fk_vd_ch_id (channel_id) REFERENCES channels(id),
    FOREIGN KEY fk_vd_py_id (playlist_id)  REFERENCES playlist(id)
);

CREATE TABLE IF NOT EXISTS distributor (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id int UNIQUE NOT NULL,
    `time` time NOT NULL,
    `date` date NOT NULL,
    `status` tinyint(1) COMMENT "0=deleted,1=avaailable,2=bannned" DEFAULT 1,
    FOREIGN KEY fk_dr_us_id (user_id) REFERENCES users(id)
);

CREATE TABLE IF NOT EXISTS views (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    video_id int NOT NULL,
    user_id int NOT NULL,
    ad tinyint(1) COMMENT "0,1" DEFAULT 1,
    channel_id int,
    `poster_id` int NOT NULL,
    `date` date NOT NULL,
    `status` tinyint(1) COMMENT "0,1" DEFAULT 1,
    ref_id int,
    FOREIGN KEY fk_vs_vd_id (video_id)  REFERENCES videos(id),
    FOREIGN KEY fk_vs_ps_id (poster_id)  REFERENCES users(id),
    FOREIGN KEY fk_vs_rf_id (ref_id)  REFERENCES distributor(id)
);

CREATE TABLE IF NOT EXISTS comments (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    video_id int NOT NULL,
    user_id int NOT NULL,
    comment varchar(225) NOT NULL,
    `type` tinyint COMMENT "0,1" DEFAULT 1,
    `at` int,
    `time` time NOT NULL,
    `date` date NOT NULL,
    `status` tinyint(1) COMMENT "0,1" DEFAULT 1,
    FOREIGN KEY fk_cm_us_id (user_id)  REFERENCES users(id),
    FOREIGN KEY fk_cm_vd_id (video_id)  REFERENCES videos(id)
);

CREATE TABLE IF NOT EXISTS likes (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    video_id int NOT NULL,
    user_id int NOT NULL,
    poster_id int NOT NULL ,
    channel_id int,
    FOREIGN KEY fk_lk_us_id (user_id)  REFERENCES users(id),
    FOREIGN KEY fk_lk_ps_id (poster_id)  REFERENCES users(id),
    FOREIGN KEY fk_lk_vd_id (video_id)  REFERENCES videos(id),
    FOREIGN KEY fk_lk_ch_id (channel_id)  REFERENCES channels(id)
);

CREATE TABLE IF NOT EXISTS dislikes (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    video_id int NOT NULL,
    user_id int NOT NULL,
    poster_id int NOT NULL ,
    channel_id int,
    FOREIGN KEY fk_dslk_us_id (user_id)  REFERENCES users(id),
    FOREIGN KEY fk_dslk_ps_id (poster_id)  REFERENCES users(id),
    FOREIGN KEY fk_dslk_vd_id (video_id)  REFERENCES videos(id),
    FOREIGN KEY fk_dslk_ch_id (channel_id)  REFERENCES channels(id)
);

CREATE TABLE IF NOT EXISTS reports (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    video_id int NOT NULL,
    user_id int NOT NULL,
    poster_id int NOT NULL,
    `date` date,
    `status` tinyint(1) COMMENT "0=waiting,1=attended" DEFAULT 0, 
    FOREIGN KEY fk_rp_us_id (user_id)  REFERENCES users(id),
    FOREIGN KEY fk_rp_ps_id (poster_id)  REFERENCES users(id),
    FOREIGN KEY fk_rp_vd_id (video_id)  REFERENCES videos(id)
);

CREATE TABLE IF NOT EXISTS money_users (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id int NOT NULL,
    channel_id int,
    paypal varchar(255),
    prev_views int,
    prev_posts int,
    `type`tinyint(1) COMMENT "0=viewer,1=blooger,2=distributor",
    `status` tinyint(1) COMMENT "0=inactive,1=active,3=banned" DEFAULT 1,
    `date` date DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY fk_mu_us_id (user_id) REFERENCES users(id),
    FOREIGN KEY fk_mu_ch_id (channel_id) REFERENCES channels(id)
);

CREATE TABLE IF NOT EXISTS subscriptions (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id int NOT NULL,
    channel_id int NOT NULL,
    `status` tinyint(1) COMMENT "0=inactive,1=active" DEFAULT 1,
    FOREIGN KEY fk_sb_us_id (user_id) REFERENCES users(id),
    FOREIGN KEY fk_sb_ch_id (channel_id) REFERENCES channels(id)
);
CREATE TABLE IF NOT EXISTS thumbnails (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    video_id int NOT NULL,
    `status` tinyint(1) COMMENT "0=inactive,1=active" DEFAULT 1,
    FOREIGN KEY fk_th_vd_id (video_id) REFERENCES videos(id)
);

