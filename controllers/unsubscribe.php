<?php

include_once '../config.php';
include_once '../helper.php';
include_once '../classes/User.php';
include_once '../classes/Video.php';
include_once '../classes/Channel.php';
include_once '../classes/Subscription.php';

$channel_id = $_POST["channel_id"];
$user_id = $_POST["user_id"];

if ($channel_id && $user_id) {
    if ($prev = Subscription::getSub($user_id, $channel_id)) {
        $subscription = Subscription::getById($prev->getId());
        $subscription->setStatus(STATUS_INACTIVE);
        $subscription->update();
    }
}