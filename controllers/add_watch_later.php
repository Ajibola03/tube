<?php

include_once '../config.php';
include_once '../helper.php';
include_once '../classes/Video.php';

$video_id = $_POST["video_id"];
if (isset($_COOKIE["watch_list"])) {
    $watch_list = json_decode($_COOKIE["watch_list"]);
} else {
    $watch_list =  [];
}

if (count($watch_list) <= 20) {
    if (array_search($video_id, $watch_list) !== False) {
        $result = -1;
    } else {
        $watch_list[] = $video_id;
        $result = 1;
    }
} else {
    $result = 0;
}

setcookie("watch_list", json_encode($watch_list), time() + (86400 * 30), "/");

echo $result;
