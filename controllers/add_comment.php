<?php

include_once '../config.php';
include_once '../helper.php';
include_once '../classes/User.php';
include_once '../classes/Money.php';
include_once '../classes/Channel.php';
include_once '../classes/Comment.php';

$comment = isset($_POST["comment"]) ? strip_tags($_POST["comment"]) : False;
$user_id = isset($_POST["user_id"]) ? $_POST["user_id"] : False;
$video_id = isset($_POST["video_id"]) ? $_POST["video_id"] : False;
$type = isset($_POST["type"]) ? $_POST["type"] : 1;
$at = isset($_POST["at"]) ? $_POST["at"] : null;
$time = date("H:i:s");
$date = date("Y/m/d");

if ($user_id && $video_id) {
    $comment = new Comment($video_id, $user_id, $comment, $type, $at, $time, $date);
    if ($comment->add()) {
        if ($type == 1) {
            $video = Video::getById($video_id);
            $comments = $video->getComments();
            foreach ($comments as $comment) {
                include '../partials/comment_row.php';
            }
        } else {
            $comment = Comment::getById($at);
            $replies = $comment->getAllReplies();
            $video = Video::getById($video_id);
            $reply = $replies[count($replies) - 1];
            if ($reply) {
//                var_dump($reply);
                include '../partials/comment_reply_row.php';
            }
        }
    }
} else {
    return "missing arguments";
}
