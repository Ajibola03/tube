<?php

include_once '../classes/User.php';
include_once '../classes/Video.php';

$i = $_POST["i"];
$id = $_POST["id"];

if($video = Video::getById($id)){
    include_once '../partials/admin/edit_video_form.php';
}
