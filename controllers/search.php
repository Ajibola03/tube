<?php

include_once '../classes/User.php';
include_once '../classes/Video.php';
include_once '../classes/Channel.php';
include_once '../classes/Like.php';
include_once '../classes/Dislike.php';

$value = $_POST["value"];
$result = Video::search($value);
if($result){
    foreach ($result as $video) {
        include '../partials/search_result.php';
    }
}else{
    echo "No Videos";
}
