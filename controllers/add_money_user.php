<?php

include_once '../config.php';
include_once '../helper.php';
include_once '../classes/User.php';
include_once '../classes/Channel.php';
include_once '../classes/Money.php';
include_once '../classes/Distributor.php';

if (isset($_SESSION["user_id"])) {
    if (!Money::getByPaypal($_POST["paypal"])) {
        $paypal = $_POST["paypal"];
        $user_id = $_POST["userId"];
        $type = $_POST["type"];
        $channel_id = Channel::getByUserId($user_id) ? Channel::getByUserId($user_id)->getId() : Null;
        // if ($type == 2) {
        //     $distributor = new Distributor($user_id, date("Y:m:d"), date("H:i:s"));
        //     if ($distributor->add()) {
        //         flash("success", "You are now a distributor");
        //     }
        // }
        if (isset($paypal) && isset($type)) {
            $money = new Money($user_id, $channel_id, $paypal, $type);
            if ($money->add()) {
                flash("success", "Account Created");
                header("location: ${BASE_URL}/money");
            } else {
                if (Money::getByPaypal($paypal)) {
                    flash("info", "Account taken");
                    header("location: ${BASE_URL}/money");
                }
                var_dump($money);
                echo '<br>';
                var_dump($money->add());
                die;
            }
        } else {
            flash("info", "Account taken");
            header("location: ${BASE_URL}/money");
        }
    } else {
        flash("info", "Account taken");
        header("location: ${BASE_URL}/money");
    }
} else {
    flash("info", "You need to login to create my money account");
    header("location: ${BASE_URL}");
}
