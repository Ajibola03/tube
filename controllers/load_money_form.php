<?php

include_once '../config.php';
include_once '../helper.php';
include_once '../classes/User.php';
include_once '../classes/Video.php';
include_once '../classes/Channel.php';
include_once '../classes/Money.php';

$type = $_POST["type"];
$allowed = [0,1,2];

if(isset($type)){
    if(in_array($type, $allowed)){
        include_once '../partials/money_reg_form.php';
    }
}