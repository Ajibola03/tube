<?php

include_once '../config.php';
include_once '../helper.php';
include_once '../classes/User.php';

$lastname = $_POST["lastname"];
$firstname = $_POST["firstname"];
$firstname = $_POST["firstname"];
$rawlocation = isset($_POST["location"]) ? $_POST["location"] : "${BASE_URL}";
$location = $rawlocation == "${BASE_URL}pages/play" ? "${BASE_URL}" : $rawlocation;
$email = $_POST["email"];
$password = password_hash($_POST["password"], 1);
$country = $_POST["country"];
$date = date("Y/m/d");
if (isset($_FILES["profile"])) {
    $target_dir = UPLOAD_PROFILE_PATH;
//    $target_file = $target_dir . basename($_FILES["profile"]["name"]);
    $uploadOkay = 1;
    $micro = explode(".", microtime())[1];
    $new_name = "PROFILE_" . $micro;
    $target_file = $target_dir . $new_name . '.' . pathinfo($_FILES["profile"]["name"], PATHINFO_EXTENSION);
    $extension = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    $allowed_ext = ["png", "jpg", "jpeg", "bmp"];
    if (!in_array($extension, $allowed_ext)) {
        $uploadOkay = 0;
    }
    if ($uploadOkay != 0 && move_uploaded_file($_FILES["profile"]["tmp_name"], $target_file)) {
        $logo_name = $new_name . '.' . pathinfo($_FILES["profile"]["name"], PATHINFO_EXTENSION);
        flash("success", "Video Uploaded Succesfully");
    }
}
$logo = isset($logo_name) ? $logo_name : "nopics.jpg";
if (User::getByEmail($email)) {
    flash("danger", "Email Taken");
    header("location:".$location);
}
$user = new User($lastname, $firstname, $email, $password, $logo, $date, $country);
if ($user->add()) {
    $user2 = User::getByEmail($user->getEmail());
    flash("success", "Welcome To Bloog View");
//    setcookie("user_id", $user->getId(), time() + (86400 * 30), "/");
    setcookie("guest", 10, time() + (-86400 * 30), "/");
    setcookie("watch_list", json_encode($watch_list), time() + (-86400 * 30), "/");
    $cookie_value = $user->getId();
    setcookie("user_id", $user->getId(), time() + (86400 * 30), "/");
//    setcookie("watch_list", json_encode($watch_list), time() + (-86400 * 30), "/");
//    $_SESSION["user_id"] = $user->getId();
    $_SESSION["user_id"] = $user2->getId();
    header("location:".$location);
}    