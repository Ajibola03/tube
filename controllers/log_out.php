<?php

include_once '../classes/User.php';

setcookie("user_id", time() + (86400 * 30), "/");
session_unset();
header("location: ../pages/account.php");
