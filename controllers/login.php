<?php

include_once '../classes/User.php';
include_once '../classes/Connection.php';
include_once '../helper.php';

$email = $_POST["email"];
$password = $_POST["password"];

if ($email && $password) {
    $user = User::getByEmail($email);
    if ($user) {
        $pass = password_verify($password, $user->getPassword());
        if ($pass) {
            $cookie_value = $user->getId();
            setcookie("user_id", $user->getId(), time() + (86400 * 30), "/");
            setcookie("watch_list", json_encode($watch_list), time() + (-86400 * 30), "/");
            $_SESSION["user_id"] = $user->getId();
            header("location: ../");
            flash("success", "Welcome" . " " . $user);
        } else {
            $wrong = True;
            header("location: ../");
            flash("error", "Wrong email or password!");
        }
    } else {
        header("location: ../");
        flash("error", "Wrong email or password!");
    }
}