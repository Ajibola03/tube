<?php

include_once '../config.php';
include_once '../helper.php';
include_once '../classes/Video.php';

$video_id = $_POST["video_id"];
$videos = False;
if (isset($_COOKIE["watch_list"])) {
    $watch_list = json_decode($_COOKIE["watch_list"]);
    $i = count(json_decode($_COOKIE["watch_list"]));
    $watch_list2 = array_remove($watch_list, $video_id);
} else {
    $result = 0;
}

if (setcookie("watch_list", json_encode($watch_list2), time() + (86400 * 30), "/")) {
    foreach (json_decode($_COOKIE["watch_list"]) as $id) {
        $videos[] = Video::getById($id);
    }
}
if ($videos) {
    include '../partials/later_line.php';
}else{
    echo 'No Videos Available';
}
