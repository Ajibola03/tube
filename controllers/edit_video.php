<?php

include_once '../classes/User.php';
include_once '../classes/Video.php';

$i = $_POST["i"];
$id = $_POST["id"];
$title = $_POST["title"];
$description = $_POST["description"];
$result = Video::getById($id);
if($result){
    $video = new Video($result->getUserId(), $result->getVideoname(), $result->getDate(), $result->getTime(), $result->getDescription(), $result->getTitle(), $result->getChannelId(), $result->getStatus(), $result->getId());
    $video->setTitle($title)->setDescription($description);
    if($video->update()){
        include_once '../partials/admin/video_row_content.php';
    }
}
