<?php
include_once '../config.php';
include_once '../classes/Connection.php';
include_once '../classes/User.php';
include_once '../classes/Video.php';
include_once '../classes/Thumbnail.php';
include_once '../classes/Channel.php';
include_once '../getID3/getid3/getid3.php';

$ffmpeg = "D:\\xampp\\ffmpeg\\bin";
$getID3 = new getID3;
$video_file = $getID3->analyze($_FILES["video"]["tmp_name"]);
$image = "test.png";
$file = $_FILES["video"]["tmp_name"];
$size = "120x90";
$sec = 5;
$cmd = "$ffmpeg -i $file -an -ss $sec -s $size $image";
$location = $_POST["location"] == "${BASE_URL}pages/play" ? "${BASE_URL}" : $_POST["location"];
$target_dir = UPLOAD_VIDEO_PATH;
$uploadOkay = 1;
$micro = explode(".",microtime())[1];
$new_name = "VID_".$micro;
$target_file = $target_dir .$new_name.'.'. pathinfo($_FILES["video"]["name"] ,PATHINFO_EXTENSION);
$extension = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
$allowed_ext = ["mp4","mkv","avi"];
$allowed_thumb_ext = ["png","jpg","jpeg"];
$resolution = [$video_file['video']["resolution_x"],$video_file['video']["resolution_y"]];

if (!in_array($extension, $allowed_ext)) {
  $uploadOkay = 0;
}
if ($uploadOkay != 0 && move_uploaded_file($_FILES["video"]["tmp_name"], $target_file)) {
    $video_name = $new_name.'.'. pathinfo($_FILES["video"]["name"] ,PATHINFO_EXTENSION);
    flash("success", "Video Uploaded Succesfully");
    if (isset($_FILES["thumb"])){
    $thumb_target_file = UPLOAD_THUMB_PATH."THUMB".$micro.".".pathinfo($_FILES["thumb"]["name"] ,PATHINFO_EXTENSION);
    $thumb_extension = strtolower(pathinfo($thumb_target_file, PATHINFO_EXTENSION));
    if (!in_array($thumb_extension, $allowed_thumb_ext)) {
        $thumb_uploadOkay = 0;
    }
    if ($uploadOkay != 0 && move_uploaded_file($_FILES["thumb"]["tmp_name"], $target_file)) {
        $thumbnail = "THUMB".$micro.".".pathinfo($_FILES["thumb"]["name"] ,PATHINFO_EXTENSION);
    }
}else{
    $cmd_video = '"'.UPLOAD_VIDEO_PATH.$video_name.'"';
    $cmd_thumb = '"'.UPLOAD_THUMB_PATH.$new_name.".png".'"';
    $time = 5;
$command = "ffmpeg -i $cmd_video -deinterlace -an -ss $time -f mjpeg -t 1 -r 1 -y -s 1280x720 $cmd_thumb";
// // "c:\\ffmpeg\\ffmpeg -i $input $output"
// var_dump(shell_exec("which php"));
// // var_dump(shell_exec("which ffmpeg"));
// var_dump($command);
// echo "<br>";
// die;
// var_dump(UPLOAD_VIDEO_PATH.$video_name);
// echo "<br>";
// // die;
exec($command . ' 2>&1', $output);
$thumbnail = $new_name.".png";

}
//    if(!shell_exec( $cmd)){
//        echo $cmd;
//        die;
//    }else{
//        echo $cmd;
//        die;
//    }

} else {
    if ($uploadOkay != 0) {
        var_dump(is_file($_FILES["video"]["name"])."<br>");
        var_dump($_FILES["video"]);
        die;
    } else {
        flash("error", "Wrong File Format") ;
    }
//    }
}
$thumb = isset($thumbnail) ? $thumbnail : "default.png";
$title = strip_tags($_POST["title"]);
$description = strip_tags($_POST["description"]);
$user = User::getById(isset($_POST["id"])?$_POST["id"]:isset($_SESSION["user_id"])?$_SESSION['user_id']:null);
$user_id = $user->getId();
if ($user->getChannel()) {
    $channel_id = $user->getChannel()->getId();
} else {
    $channel_id = null;
}
$date = date("Y/m/d");
$time = date("H:i:s");

$video = new Video($user_id, $video_name, $date, $time, $description, $title, $channel_id);
//$real_thumbnail = new Thumbnail($name, $video_id);
if ($video) {
    $video->add();
    $video_id = Video::getIdFromName($video_name);
    $real_thumbnail = new Thumbnail($thumb, $video_id);
    $real_thumbnail->add();
    flash("success", "Video Uploaded Succesfully");
    header("location:".$location);
}else{
    var_dump(is_file($_FILES["video"]["name"])."<br>");
    var_dump($_FILES["video"]);
    die;
}