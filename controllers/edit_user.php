<?php

include_once '../classes/User.php';

$i = $_POST["i"];
$id = $_POST["id"];
$lastname = $_POST["lastname"];
$firstname = $_POST["firstname"];
$email = $_POST["email"];
$country = $_POST["country"];

if($result = User::getById($id)){
    $user = new User($result->getLastname(), $result->getFirstname(), $result->getEmail(), $result->getPassword(), $result->getLogo(), $result->getDate(), $result->getCountry(), $result->getStatus(), $result->getId());
    $user->setLastname($lastname)->setFirstname($firstname)->setEmail($email)->setCountry($country);
    if($user->update()){
        include_once '../partials/admin/user_row_content.php';
    }
}
