<?php

include_once '../classes/User.php';
include_once '../classes/Video.php';
include_once '../classes/Channel.php';
include_once '../classes/View.php';

$video_id = $_POST["video_id"];
$user_id = isset($_SESSION["user_id"]) ? $_SESSION["user_id"] : 0;
$add = isset($_SESSION["ad"]) ? $_SESSION["ad"] : 1;
$ad = $add > 1 ? 0 : $add;
$video = Video::getById($video_id);
$poster_id = $video->getUserId();
$date = date("Y:m:d");
$ref_id = isset($_POST["ref_id"]) ? $_POST["ref_id"] : null;

if ($video->getChannel() === True) {
    $channel_id = $video->getChannelId();
} else {
    $channel_id = null;
}
$view = new View($video_id, $user_id, $ad, $channel_id, $poster_id, $date, $ref_id);
if ($view) {
    if (isset($_SESSION["user_id"])) {
        if (View::checkViewed($video_id, $_SESSION["user_id"])) {
            header("location: ${BASE_URL}pages/account_.php");
        } else {
            $view->add();
            header("location: ${BASE_URL}pages/account_.php");
        }
    } else {
        $view->add();
        header("location: ${BASE_URL}pages/account_.php");
    }
}
return $view;