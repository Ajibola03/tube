<?php

include_once '../config.php';
include_once '../helper.php';
include_once '../classes/Connection.php';
include_once '../classes/Video.php';

$video = Video::getById($_POST["link"]);
$ref_id = isset($_POST["ref_id"]) ? $_POST["ref_id"] : null;
$low_res = $_POST["low_res"];
$time = $_POST["time"];
$_SESSION["video_id"] = 1;
$_SESSION["ad"] = ($time > 10) ? 1 : 0;
$_SESSION["current_video"] = $_POST["link"];

if($video){
    if($low_res == 1){
        include_once '../partials/low_vid_row.php';
    }else{
    include_once '../partials/vid_row.php';
    }
}
