<?php

//var_dump(cal_days_in_month(0, 12, 2019));
//die;
include_once '../config.php';
include_once '../helper.php';
include_once '../classes/User.php';
include_once '../classes/Money.php';
include_once '../classes/Channel.php';

$month = date("M");
$ext = ".txt";
$id = $_POST["id"];
$money_account = Money::getById($id);
$paypal = isset($_POST["email"]) ? $_POST["email"] : $money_account->getPaypal();
$amount = isset($_POST["amount"]) ? $_POST["amount"] : $money_account->getAmountAvl();
$currency = isset($_POST["currency"]) ? $_POST["currency"] :"USD";
$break = "\r\n";
$space = " ";

if ($amount){
$file = fopen(PAYMENT_FILE_PATH.$month."_"."payment".$ext, "a+", False);
$content = $paypal.$space.$amount.$space.$currency.$break;

fwrite($file, $content);
fclose($file);
echo "1";
}else{
	echo "2";
}

