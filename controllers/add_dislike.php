<?php

include_once '../classes/User.php';
include_once '../classes/Video.php';
include_once '../classes/Channel.php';
include_once '../classes/Like.php';
include_once '../classes/Dislike.php';

$video_id = $_POST["video_id"];
$user_id = $_POST["user_id"];
$user = User::getById($user_id);
$video = Video::getById($video_id);
$poster_id = $video->getUserId();
if ($video->getChannel()) {
    $channel_id = $video->getChannelId();
} else {
    $channel_id = null;
}
if (!Dislike::getByUserId($user_id,$video_id)) {
    $dislike = new Dislike($video_id, $user_id, $poster_id, $channel_id);
    $result = $dislike->add();
}
include_once '../partials/video_details.php';
