<?php
include_once 'config.php';
include_once 'partials/navbar.php';
include_once 'partials/sidebar.php';
?>
<div class="content d-inline-flex float-right bg-grey" style="height: 90vh">
    <div class="container-fluid">
        <?php
        if (!isset($_SESSION["user_id"])) {
            include_once 'partials/no_money.php';
        } else {
            if (Money::getByUserId($_SESSION["user_id"])) {
//                var_dump(Money::getByUserId($_SESSION["user_id"]));
//                die;
                $money = Money::getByUserId($_SESSION["user_id"]);
                $user = User::getById($_SESSION["user_id"]);
                include_once 'partials/blooger_my_money.php';
            } else {
                $user = User::getById($_SESSION["user_id"]);
                include_once 'partials/no_money.php';
            }
        }
        ?>
    </div>
</div>
<div id="modal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary text-center">
                <div class="modal-tile">
                    <h5 class="text-center text-white" style="text-align: center"><i class="fa fa-info-circle mr-5" style="color:white;"></i>BloogView Money</h5>
                </div>
            </div>
            <div class="modal-body">
                <?php if (isset($_SESSION["user_id"])){ ?>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-pills nav-justified">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#blooger">Make Money As Blooger</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#viewer">Make Money As Viewer</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#distributor">Make Money As Distributor</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active container" id="blooger">
                        <p><b class="py-3">Blooger Info</b><br>
                            Note: If you create a Blooger’s Money Account you get paid only for Services render as a Blooger.
                            Requirments:<br>
                            1.	A PayPal account, where you will receive money</p>
                        <div class="btn btn-sm btn-primary float-right" onclick="fetchMoneyRegForm(1)">Sign Up</div>
                    </div>
                    <div class="tab-pane container" id="viewer">
                        <p><b class="py-3">Viewer Info</b><br>
                            Note: If you create a Viewer’s Money Account you get paid only for Services render as a Viewer.
                            Requirments:<br>
                            1.	A PayPal account, where you will receive money..</p>
                        <div class="btn btn-sm btn-primary float-right" onclick="fetchMoneyRegForm(0)">Sign Up</div>
                    </div>
                    <div class="tab-pane container" id="distributor">
                        <p><b class="py-3">Viewer Info</b><br>
                            Note: If you create a Viewer’s Money Account you get paid only for Services render as a Distributor.
                            Requirments:<br>
                            1.	A PayPal account, where you will receive money..</p>
                        <div class="btn btn-sm btn-primary float-right" onclick="fetchMoneyRegForm(2)">Sign Up</div>
                    </div>
                </div>
                <?php } else { ?>
                <i>You need to login to register</i>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!-- jquery -->
<script src="<?= BASE_URL ?>dist/js/jquery-3.4.1.js"></script>
<!-- popper -->
<script src="<?= BASE_URL ?>dist/js/popper.min.js"></script>
<script type="text/javascript">
                            $(document).ready(function () {
<?php if (!isset($_COOKIE["money_modal"])) { ?>
                                    $('#modal').modal('show');
                                    destroyOneTimer("money_modal");
<?php } ?>
                            });
</script>
<?php include_once 'partials/footer.php'; ?>

