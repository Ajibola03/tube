<?php
define("BASE_PATH",  __DIR__);

$error = "error";

function str_replace_last($search, $replace, $str) {
    return ($pos = strrpos($str, $search) ) !== false ? substr_replace($str, $replace, $pos, strlen($search)) : $str;
}

function baseURL($path = "", $includeProtocol = true) {
    $baseUrl = "";
    if ($includeProtocol) {
        $baseUrl = "http://";
        // if ($_SERVER["SERVER_PORT"] == "443" || $_SERVER["REQUEST_SCHEME"] == "https" || $_SERVER["HTTPS"] == "on") {
        if ($_SERVER["SERVER_PORT"] == "443" || $_SERVER["REQUEST_SCHEME"] == "https") {
            $baseUrl = "https://";
        }
        $baseUrl .= $_SERVER["SERVER_NAME"];
    }
    $dirItems = explode(DIRECTORY_SEPARATOR, BASE_PATH);
    $folderName = $dirItems[count($dirItems) - 1];
    //check if the folderName is in requesr uri
    if (($dirPos = stripos($_SERVER["REQUEST_URI"], $folderName)) && $dirPos !== false && false === stripos($_SERVER["REQUEST_URI"], $path)) {
        $baseUrl .= substr($_SERVER["REQUEST_URI"], 0, $dirPos + strlen($folderName));
    }
    $path = trim($path, " \t\n\r\0\x0B\/");
//    echo "1 - baseURL: \$path = $path\n";
    $path = str_ireplace(str_replace(DIRECTORY_SEPARATOR, "/", BASE_PATH), "", $path);
//    echo "2 - baseURL: \$path = $path\n";
    return strlen($path) > 0 ? ($baseUrl .= "/" . $path) : $baseUrl;
}


function pathToURL($filePath) {
//    echo "BASE_PATH = ".BASE_PATH."\n";
    $basePath = BASE_PATH;
    if (trim($basePath) == "") {
        $basePath = realpath(__DIR__ . DIRECTORY_SEPARATOR . "..");
    }
//    echo "\$filePath = $filePath\n";
    $filePath = str_replace("/", DIRECTORY_SEPARATOR, $filePath);
//    echo "\$filePath = $filePath\n";
    $basename = basename($filePath);
    $path = realpath(str_replace_last($basename, "", $filePath));
    $rootPath = $basePath;
    $path = trim(str_replace($rootPath, "", $path), "\t\n\r\0\x0B\/\\");
    $path = str_replace(DIRECTORY_SEPARATOR, "/", $path) . "/" . $basename;
    return baseURL($path);
}

function flash($type, $message = null) {
    ?>
    <script type="text/javascript">
    <?php
    if ($message) {
        $_SESSION["message"] = $message;
        $_SESSION["type"] = $type;
        if (isset($_SESSION["message"]) && $_SESSION["message"] != "set") {
            ?>
                flash(<?= json_encode($_SESSION["type"]) ?>, <?= json_encode($_SESSION["message"]) ?>);
            <?php
        }
    } elseif ($_SESSION["message"] && $_SESSION["message"] != "set") {
        ?>
            flash(<?= json_encode($_SESSION["type"]) ?>, <?= json_encode($_SESSION["message"]) ?>);
        <?php
        $_SESSION["message"] = null;
    } else {
        $_SESSION["message"] = "set";
    }
    ?>
    </script>
    <?php
}

function array_remove($array, $element) {
    $index = array_search($element, $array);
    $split = array_splice($array, $index, 1);
    $result = array_diff($array, $split);
    return $result;
}

function time_diff($date, $full = False) {
    $date1 = date_create($date);
    $date2 = date_create(date("Y-m-d") . " " . date("H:i:s"));
    $interval = date_diff($date1, $date2);
    $value = 0;
    $array = [$interval->y, $interval->m, $interval->d, $interval->h, $interval->i, $interval->s];
    $unit = ["year", "month", "day", "hour", "min", "second"];
    if ($interval->invert == 1) {
        return False;
    }
    if ($array == [0, 0, 0, 0, 0, 0]) {
        return "just now";
    }
    for ($i = 0; $i < count($array); $i++) {
        if ($value > 0)
            continue;
        $ext = $unit[$i];
        if ($array[$i] > 0) {
            $value = $array[$i];
            if ($array[$i] > 1) {
                $ext = $unit[$i] . "s";
            }
        }$value = $value ." ". $ext;
    }return "about" . " " . $value . " " . "ago";
}

function getExtractString(){
    $today = date("d");
    $month = date("m");
    $year = date("Y");
    $target = cal_days_in_month(0, $month, $year);
    if($today != $target){
        return "disabled";
    }
}

function getDaysLeft(){
    $today = date("d");
    $month = date("m");
    $year = date("Y");
    $target = cal_days_in_month(0, $month, $year);
    if($today != $target){
        return $target - $today == 1 ? $target - $today." day left" : $target - $today." days left";
    }else{
        return "0 days left";
    }
}

function getDaysLeftVal(){
    $today = date("d");
    $month = date("m");
    $year = date("Y");
    $target = cal_days_in_month(0, $month, $year);
    if($today != $target){
        return true;
    }else{
        return false;
    }
}

 function resize_crop_image($max_width, $max_height, $source_file, $dst_dir, $quality = 80){
    $imgsize = getimagesize($source_file);
    $width = $imgsize[0];
    $height = $imgsize[1];
    $mime = $imgsize['mime'];
    switch($mime){
        case 'image/gif':
            $image_create = "imagecreatefromgif";
            $image = "imagegif";
            break;
 
        case 'image/png':
            $image_create = "imagecreatefrompng";
            $image = "imagepng";
            $quality = 7;
            break;
 
        case 'image/jpeg':
            $image_create = "imagecreatefromjpeg";
            $image = "imagejpeg";
            $quality = 80;
            break;
 
        default:
            return false;
    }
    $dst_img = imagecreatetruecolor($max_width, $max_height);
    $src_img = $image_create($source_file);
    $width_new = $height * $max_width / $max_height;
    $height_new = $width * $max_height / $max_width;
    //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
    if($width_new > $width){
        //cut point by height
        $h_point = (($height - $height_new) / 2);
        //copy image
        imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
    }else{
        //cut point by width
        $w_point = (($width - $width_new) / 2);
        imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
    }
    $image($dst_img, $dst_dir, $quality);
    if($dst_img){imagedestroy($dst_img);}
    if($src_img){imagedestroy($src_img);}
//    resize_crop_image(20, 20, $image, UPLOAD_VIDEO_PATH ."cut".'.'. pathinfo($_FILES["video"]["name"] ,PATHINFO_EXTENSION));
}

function translate($from_lan, $to_lan, $text){
    $json = json_decode(file_get_contents('https://ajax.googleapis.com/ajax/services/language/translate?v=1.0&q=' . urlencode($text) . '&langpair=' . $from_lan . '|' . $to_lan));
    $translated_text = $json->responseData->translatedText;

    return $translated_text;
}

function autoGenerateTranslationFile($obj){

}

function minify_html($buffer) {

    $search = array(
        '/\>[^\S ]+/s',     // strip whitespaces after tags, except space
        '/[^\S ]+\</s',     // strip whitespaces before tags, except space
        '/(\s)+/s',         // shorten multiple whitespace sequences
        '/<!--(.|\s)*?-->/' // Remove HTML comments
    );

    $replace = array(
        '>',
        '<',
        '\\1',
        ''
    );

    $buffer = preg_replace($search, $replace, $buffer);

    return $buffer;
}


