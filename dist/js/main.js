
if (screen.width > 1000) {
    width = 250;
} else {
    width = 0;
}

var show = true;
var prev;
function toggleSidebar() {
    if (width === 250) {
        $(".side-bar").animate({
            width: "6%"
        });
        $(".content").animate({
            width: "94%"
        });
        $(".icon-text").removeClass("d-none");
        $(".bars").toggleClass("d-none");
        $(".large").removeClass("d-inline");
        $(".large").addClass("d-none");
        $(".side-bar").addClass("text-center");
        $(".normal-text").addClass("d-none");
        width = 10;
    } else if (width === 0) {
        $(".side-bar").animate({
            width: "200px"
        });
        $(".shadow").removeClass("d-none");
        $("body").addClass("block");
        $(".bars").toggleClass("d-none");
        width = 200;
    } else if (width === 200) {
        $(".side-bar").animate({
            width: "-=200px"
        });
        $(".bars").toggleClass("d-none");
        $(".shadow").addClass("d-none");
        $("body").removeClass("block");
        width = 0;
    } else {
        $(".content").animate({
            width: "84%"
        });
        $(".side-bar").animate({
            width: "16%"
        });
        $(".icon-text").addClass("d-none");
        $(".bars").toggleClass("d-none");
        $(".large").removeClass("d-none");
        $(".large").addClass("d-inline");
        $(".side-bar").removeClass("text-center");
        $(".normal-text").removeClass("d-none");
        width = 250;
    }
}
function hideSidebar() {
    $(".side-bar").animate({
        width: "-=200px"
    });
    $(".bars").toggleClass("d-none");
    $(".shadow").addClass("d-none");
    $("body").removeClass("block");
    width = 0;
}

function toggleSearch() {
    if (show) {
        $(".hidden-search").removeClass("d-none");
        $(".hidden-search").animate({
            width: "100%"
        });
        show = false
    } else {
        $(".hidden-search").animate({
            width: "0%"
        });
        $(".hidden-search").addClass("d-none");
        show = true
    }
}

function showClip(video) {
    prev = $("#img1").attr("src");
    window.setInterval(
        function () {
            $("#img1").attr("src", "images/" + video);
        }, 2000)
    //    $("#img1").attr("src", "images/"+video);
    console.log(prev)
}

function endClip() {
    $("#img1").attr("src", prev);
    prev = prev;
}

function displayName(id, html) {
    var name = $("#" + id).val();
    name = name.split("\\")[2];
    $("#" + html).html(name);
}

$(".form-pass").keyup(function () {
    var pass1 = $("#password").val();
    var pass2 = $("#password2").val();
    if (pass1.split("").length < 8) {
        $("#sub").attr("disabled", "true");
        $(".validate-length").removeClass("d-none")
    } else {
        $("#sub").removeAttr("disabled");
        $(".validate-length").addClass("d-none")
    }
    if ($("#password").val() != "" && pass1.split("").length >= 8) {
        if (pass1 != pass2) {
            $(".validate-length").addClass("d-none")
            $("#sub").attr("disabled", "true");
            $(".validate").removeClass("d-none")
        } else {
            $("#sub").removeAttr("disabled");
            $(".validate").addClass("d-none")
        }
    }
});
$("#password2").click(function () {
    if ($("#password").val() == "") {
        $("#password").focus();
    }
});
function loadEditUser(i, id) {
    $.ajax({
        url: "../../controllers/load_edit_user.php",
        type: "post",
        data: {
            i: i,
            id: id
        },
        success: function (res) {
            $("#editUserModal").modal('show');
            $("#editUser").html(res);
        }
    });
}

function loadEditVideo(i, id) {
    $.ajax({
        url: "../../controllers/load_edit_video.php",
        type: "post",
        data: {
            i: i,
            id: id
        },
        success: function (res) {
            $("#editUserModal").modal('show');
            $("#editUser").html(res);
        }
    });
}

function editUser(i, id) {
    var lastName = $("#lastname").val();
    var firstName = $("#firstname").val();
    var country = $("#country").val();
    var email = $("#email").val();
    $.ajax({
        url: "../../controllers/edit_user.php",
        type: "post",
        data: {
            i: i,
            id: id,
            lastname: lastName,
            firstname: firstName,
            country: country,
            email: email
        },
        success: function (res) {
            $("#editUserModal").modal('hide');
            $("#user" + i).html(res);
            console.log("#user" + i);
        }
    });
}

function editVideo(i, id) {
    var title = $("#title").val();
    var description = $("#description").val();
    $.ajax({
        url: "../../controllers/edit_video.php",
        type: "post",
        data: {
            i: i,
            id: id,
            title: title,
            description: description
        },
        success: function (res) {
            $("#editUserModal").modal('hide');
            $("#video" + i).html(res);
            console.log("#video" + i);
        }
    });
}

function playVideo(id) {
    if (id) {
        $.ajax({
            url: "../../controllers/play_video.php",
            type: "post",
            data: {
                id: id
            },
            success: function (res) {
                $("li.active").removeClass("active");
                $(".content").html(res);
            }
        });
    }
    console.log("error: Unidentified index i");
}

function addLike(userId, videoId) {
    if (userId && videoId) {
        $.ajax({
            url: "../../controllers/add_like.php",
            type: "post",
            data: {
                user_id: userId,
                video_id: videoId
            },
            success: function (res) {
                $("#first").html(res);
            }
        });
    }
    console.log("error: Unidentified index i");
}

function addDislike(userId, videoId) {
    if (userId && videoId) {
        $.ajax({
            url: "../../controllers/add_dislike.php",
            type: "post",
            data: {
                user_id: userId,
                video_id: videoId
            },
            success: function (res) {
                $("#first").html(res);
            }
        });
    }
    console.log("error: Unidentified index i");
}

function report(email) {
    if (email) {
        $.ajax({
            url: "../../controllers/mail.php",
            type: "post",
            data: {
                email: email
            },
            success: function (res) {
                //                $(".card").html(res);
                $(".content").append(res);
            }
        });
    }
    console.log("error: Unidentified index i");
}

function subscribe(user_id, channel_id) {
    if (user_id && channel_id) {
        $.ajax({
            url: "../../controllers/subscribe.php",
            type: "post",
            data: {
                user_id: user_id,
                channel_id: channel_id
            },
            success: function () {
                $(".sub-btn").toggleClass("d-none");
                flash("success", "subscribed")
            }
        });
    } else {
        console.log("error: Unidentified index i");
    }
}
function unsubscribe(user_id, channel_id) {
    if (user_id && channel_id) {
        $.ajax({
            url: "../../controllers/unsubscribe.php",
            type: "post",
            data: {
                user_id: user_id,
                channel_id: channel_id
            },
            success: function () {
                //                $(".content").append(res);
                $(".sub-btn").toggleClass("d-none");
                flash("success", "unsubscribed")
            }
        });
    } else {
        console.log("error: Unidentified index i");
    }
}

function toggleBarVisibility() {
    var e = document.getElementById("bar_blank");
    e.style.display = (e.style.display == "none") ? "block" : "block";
}

function createRequestObject() {
    var http;
    if (navigator.appName == "Microsoft Internet Explorer") {
        http = new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        http = new XMLHttpRequest();
    }
    return http;
}

function sendRequest() {
    var http = createRequestObject();
    http.open("GET", "progress.php");
    http.onreadystatechange = function () {
        handleResponse(http);
    };
    http.send(null);
}

function handleResponse(http) {
    var response;
    if (http.readyState == 4) {
        response = http.responseText;
        document.getElementById("bar_color").style.width = response + "%";
        document.getElementById("status").innerHTML = response + "%";
        if (response < 100) {
            setTimeout("sendRequest()", 1000);
        } else {
            toggleBarVisibility();
            document.getElementById("status").innerHTML = "Done.";
            //            flash("success","file Uploaded");
        }
    }
}

function startUpload() {
    toggleBarVisibility();
    setTimeout("sendRequest()", 1000);
}

function SUBTIM() {
    document.getElementById("myForm").onsubmit = startUpload;
}
;
function displayTime(id) {
    var vid = document.getElementById(id);
    console.log(vid.currentTime);
}

function startAdTimer() {
    //    var vid = document.getElementById("example_video_1");
    var vid = videojs('example_video_1');
    var time;
    var timeleft;
    console.log(vid.currentTime());
    setInterval(function () {
        time = vid.currentTime();
        time = time.toFixed(0);
        if (time > 5) {
            $(".skip.d-none").removeClass("d-none");
            $("#timeleft").html(30 - time);
            //            positionButtons();
            timeleft = 30 - time;
            if (timeleft < 0) {
                $(".timer").addClass("d-none");
                //                $(".skip").addClass("d-none");
            }
        }
    }, 50);
    //            alert("ready");

}

function positionButtons() {
    var nav = $(".nav-bar");
    var vid = $("#example_video_1");
    var self = $(".skip");
    var nav_height = nav.outerHeight();
    var vid_height = vid.outerHeight();
    var vid_width = vid.outerWidth();
    var self_height = self.outerHeight();
    var self_width = self.outerWidth();
    var total = (nav_height + vid_height) - (self_height * 2);
    if (screen.width > 1024) {
        $(".skip").animate({
            top: total,
            left: vid_width + self_width
        });
    } else {
        $(".skip").animate({
            top: total,
            left: vid_width
        });
    }
}

function skipAd(url) {
    var vid = document.getElementById("example_video_1");
    var time;
    time = vid.currentTime;
    //    time = time.toFixed(0);
    console.log(vid);
    console.log($("#playerSrc").attr("src", url));
    //    vid.pause();
}

function changeVid(link, lowRes, ref) {
    //    var vid = document.getElementById("example_video_1");
    var vid = videojs("example_video_1");
    var time;
    time = vid.currentTime();
    time = time.toFixed(0);
    console.log(lowRes);
    if (link) {
        $.ajax({
            url: "../../controllers/skip_ad.php",
            type: "post",
            data: {
                link: link,
                time: time,
                low_res: lowRes,
                ref_id: ref
            },
            success: function (res) {
                $("#vid").html(res)
                console.log($('#vid').find('.video-js')[0])
                videojs($('#vid').find('.video-js')[0]).ready(function () {
                    var myPlayer = videojs($('#vid').find('.video-js')[0]);
                    //                    myPlayer.destroy();
                    myPlayer.play();
                });
            }
        });
    } else {
        console.log("error: Unidentified index i");
        $(".timer").addClass("d-none");
        $(".skip").addClass("d-none");
    }
}

function search(id) {
    var value = $("#search" + id).val();
    if (value !== '') {
        $.ajax({
            url: "../../controllers/search.php",
            type: "post",
            data: {
                value: value
            },
            success: function (res) {
                $(".search_results").removeClass("d-none");
                $(".shadow-blur").removeClass("d-none");
                $("#result" + id).html(res);
                $("#topic" + id).html("search result for" + " " + value)
            }
        });
    } else {
        $(".search_results").addClass("d-none");
    }
}

function addToWatchLater(id) {
    $.ajax({
        url: "../../controllers/add_watch_later.php",
        type: "post",
        data: {
            video_id: id
        },
        success: function (res) {
            console.log(res);
            if (res == 0) {
                flash("info", "watch list full")
            } else if (res == 1) {
                flash("success", "Added succesfully")
            } else {
                flash("info", "already exists")
            }
        }
    });
}

function clearWatchLater(id = null) {
    $.ajax({
        url: "../../controllers/clear_watch_later.php",
        type: "POST",
        data: {
            id: id
        },
        success: function (res) {

        }
    })
}
function removeWatchLater(id) {
    $.ajax({
        url: "../../controllers/remove_watch_later.php",
        type: "post",
        data: {
            video_id: id
        },
        success: function (res) {
            $("#later_block").html(res)
        }
    });
}

function addView(id, ref) {
    $.ajax({
        url: "../../controllers/add_view.php",
        type: "post",
        data: {
            video_id: id,
            ref_id: ref
        }
    });
}

function fetchMoneyRegForm(id) {
    $.ajax({
        url: "../../controllers/load_money_form.php",
        type: "post",
        data: {
            type: id
        },
        success: function (res) {
            $(".content").append(res);
            $('#modal').modal('hide');
            $("#regMoneyModal").modal('show');
        }
    });
}

function destroyOneTimer(id) {
    console.log(id);
    $.ajax({
        url: "../../controllers/destroy_one_timer.php",
        type: "post",
        data: {
            id: id
        },
        success: function (res) {

        }
    });
}

function toTitleCase(string) {
    //    name = string.split(".")[0];
    array = string.split("");
    stray = string.split("");
    first = array[0];
    rest_array = stray.splice(1, stray.length - 1);
    rest = rest_array.join("");
    result = first.toUpperCase() + rest;
    //    console.log(result);
    return result;
}

function toTitle(string) {
    string = string.split(".php")[0];
    if (string.search("_") > 1) {
        one = string.split("_")[0];
        two = string.split("_")[1];
        result = toTitleCase(one) + " " + toTitleCase(two);
        res = "Bloog View | " + result;
    } else {
        result = toTitleCase(string);
        res = "Bloog View | " + result;
    }
    if (result == "Sub") {
        res = "Bloog View | Subscription";
    }
    $("#titlebar").html(res);
}

function comment(user_id, video_id, comment, type = 1, at = null) {
    if (user_id && video_id && comment) {
        $.ajax({
            url: "../../controllers/add_comment.php",
            type: "post",
            data: {
                user_id: user_id,
                video_id: video_id,
                comment: comment,
                type: type,
                at: at
            },
            success: function (res) {
                if (type === 1) {
                    $("#comment-area").html(res);
                } else {
                    $("#replies" + at).prepend(res);
                    console.log("#replies" + at);
                }
                $(".content").append(flash("success", "done"));
                $(".comment").val('');
            }
        });
    }
    $(".comment").val('');
}

function addToExtract(id) {
    $.ajax({
        url: "../../controllers/extract.php",
        type: "POST",
        data: ({
            id: id
        }),
        success: function (res) {
            if (res == 1) {
                flash("success", "done");
            } else if (res == 2) {
                flash("info", "Nothing to extract");
            } else {
                flash("error", "error");
            }
        }
    });
}

function deleteVideo(userId, videoId) {
    $.ajax({
        url: "../../controllers/delete_video.php",
        type: "post",
        data: ({
            user_id: userId,
            video_id: videoId
        }),
        success: function (res) {
            $("content").append(res);
        }
    });
}

function flash(type, message) {
    if (type && message) {
        if (type === "success") {
            toastr.success(message);
        } else if (type === "info") {
            toastr.info(message);
        } else if (type === "danger") {
            toastr.error(message);
        } else if (type === "error") {
            toastr.error(message);
        } else if (type === "warning") {
            toastr.warning(message);
        } else {
            toastr.warning(message);
        }
    }
}

$(function () {
    //Initialize Select2 Elements
    $('.select2').select2();

    //Initialize Select2 Elements
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    });
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox();

    $(".star").attr("required", "true");
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: true,
        timer: 3000
    });

});