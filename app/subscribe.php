<?php

include_once '../config.php';
include_once '../helper.php';
include_once '../classes/User.php';
include_once '../classes/Video.php';
include_once '../classes/Channel.php';
include_once '../classes/Subscription.php';

if( isset($_POST["user_id"]) && isset($_POST["channel_id"]) ){
$channel_id = $_POST["channel_id"];
$user_id = $_POST["user_id"];
$complete = false;

if ($channel_id && $user_id) {
    if ($prev = Subscription::getSub($user_id, $channel_id)) {
        $subscription = Subscription::getById($prev->getId());
        $subscription->setStatus(STATUS_ACTIVE);
        if($subscription->update()){
        	$complete = True;
        }
    } else {
        $subscription = new Subscription($user_id, $channel_id);
        if($subscription->add()){
        	$complete = True;
        }
    }
}
header('Content-Type: application/json');
echo(json_encode($complete));

}