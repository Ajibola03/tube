<?php

include_once '../classes/Connection.php';
include_once '../classes/User.php';
include_once '../classes/Video.php';
include_once '../classes/Comment.php';
include_once '../classes/Thumbnail.php';
include_once '../classes/Channel.php';
include_once '../getID3/getid3/getid3.php';

class NewComment extends Connection {

    public $id, $video_id, $user_id, $comment, $type, $at, $reply, $time, $date, $status, $time_diff, $image;

    function __construct($id, $video_id, $user_id, $comment, $type, $at, $time, $date, $status, $reply, $time_diff, $image) {
        $this->id = $id;
        $this->video_id = $video_id;
        $this->user_id = $user_id;
        $this->comment = $comment;
        $this->type = $type;
        $this->at = $at;
        $this->time = $time;
        $this->date = $date;
        $this->status = $status;
        $this->reply = $reply;
        $this->time_diff = $time_diff;
        $this->image = $image;
    }
}
if (isset($_POST['video_id'])){
	$video_id = $_POST['video_id'];
	$video = Video::getById($video_id);
	$comments = $video->getComments();
	$real_comments = [];
	foreach ($comments as $comment) {
		$real_id = $comment->getId();
		$real_video_id = $comment->getVideoId();
		$real_user_id = $comment->getUserId();
		$real_comment = $comment->getComment();
		$real_type = $comment->getStringType();
		$real_at = $comment->getAt();
		$real_time = $comment->getTime();
		$real_date = $comment->getDate();
		$real_status = $comment->getStatus();
		$replies = $comment->getAllReplies();
		$real_reply = [];
		foreach ($replies as $reply) {
			$reply_user = User::getById($comment->getUserId());
		$reply_logo = $reply_user->getLogo();
		$reply_image = BASE_URL."uploads/profiles/".$reply_logo;
			$real_reply[] = new NewComment($reply->getId(), $reply->getVideoId(), $reply->getUserId(), $reply->getComment(), $reply->getStringType(), $reply->getAt(), $reply->getTime(), $reply->getDate(), $reply->getStatus(), $reply->getAllReplies(), $reply->getTimeDiff(), $reply_image);
		}
		$real_time_diff = $comment->getTimeDiff();
		$real_user = User::getById($comment->getUserId());
		$real_logo = $real_user->getLogo();
		$real_image = BASE_URL."uploads/profiles/".$real_logo;

		$real_comments[] = new NewComment($real_id, $real_video_id, $real_user_id, $real_comment, $real_type, $real_at, $real_time, $real_date, $real_status, $real_reply, $real_time_diff, $real_image);
	}

	header('Content-Type: application/json');
	echo(json_encode($real_comments));
}
