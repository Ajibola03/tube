<?php

include_once '../config.php';
include_once '../classes/User.php';
include_once '../getID3/getid3/getid3.php';

class NewUser extends Connection{
	public $id, $lastname, $firstname, $email, $password, $logo, $date, $country, $status, $conn;

    function __construct($id, $lastname, $firstname, $email, $password, $logo, $date, $country, $status) {
        $this->id = $id;
        $this->lastname = $lastname;
        $this->firstname = $firstname;
        $this->email = $email;
        $this->logo = $logo;
        $this->status = $status;
        $this->date = $date;
        $this->country = $country;
        $this->password = $password;
        $this->conn = new Connection;
    }
}

if (isset($_POST["token"])) {
	# code...
	$obj = json_decode($_POST["body"]);
	$lastname = $obj->lastname;
	$firstname = $obj->firstname;
	$email = $obj->email;
	$password = password_hash($obj->password, PASSWORD_DEFAULT);
	$logo = "nopics.jpg";
	$date = date("Y/m/d");
	$country = "Nigeria";

	$data = new User($lastname, $firstname, $email, $password, $logo, $date, $country);
	$result = $data->add() ? True : $data->add();
	if($result){
		$real_id = User::getLastId();
		$real_lastname = $data->getLastname();
		$real_firstname = $data->getFirstname();
		$real_email = $data->getEmail();
		$real_password = $data->getPassword();
		$real_logo = $data->getLogo();
		$real_date = $data->getDate();
		$real_status = $data->getStatus();
		$real_country = $data->getCountry();

		$user = new NewUser($real_id, $real_lastname, $real_firstname, $real_email, $real_password,
		 $real_logo, $real_date, $real_country, $real_status);
	}
	header('Content-Type: application/json');
	echo(json_encode($user));
}