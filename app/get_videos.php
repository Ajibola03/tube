<?php

include_once '../config.php';
include_once '../classes/Video.php';
include_once '../classes/Subscription.php';
include_once '../getID3/getid3/getid3.php';

if(isset($_POST["token"]) && isset($_POST["video_ids"])){

$video_ids = json_decode($_POST["vidoe_ids"]);
$videos = [];
foreach ($video_ids as $video_id) {
	$videos[] = Video::getById($video_id);
}
// $videos = Video::getAll();
$reqId = isset($_POST["user_id"]) ? $_POST["user_id"] != 0 ? $_POST["user_id"] : False : False;
// $getID3 = new getID3;
class NewVideo extends Connection {

    public $id, $user_id, $channel_id, $video_name, $date, $time, $status,
            $description, $title, $playlist_id, $likes, $dislikes, $views, $ago, $thumb,
            $channel, $logo, $low_res, $is_sub, $friendy_description;

    function __construct($id, $user_id, $video_name, $date, $time, $description,
            $title, $channel_id, $playlist_id, $status, $likes, $dislikes, $views, $ago, 
            $thumb, $channel, $logo, $low_res, $is_sub, $friendy_description) {
        $this->id = $id;
        $this->user_id = $user_id;
        $this->playlist_id = $playlist_id;
        $this->channel_id = $channel_id;
        $this->channel = $channel;
        $this->video_name = $video_name;
        $this->date = $date;
        $this->time = $time;
        $this->status = $status;
        $this->description = $description;
        $this->title = $title;
        $this->likes = $likes;
        $this->dislikes = $dislikes;
        $this->views = $views;
        $this->ago = $ago;
        $this->thumb = $thumb;
        $this->logo = $logo;
        $this->low_res = $low_res;
        $this->is_sub = $is_sub;
        $this->friendy_description = $friendy_description;
        $this->conn = new Connection;
    }

  }

foreach ($videos as $value) {
	# code...
$video = Video::getById($value->getId());
$real_views = $video->getViews();
$real_likes = $video->getLikes();
$real_dislikes = $video->getDislikes();
$real_Channel = $video->getChannelName();
$real_id = $video->getId();
$real_user_id = $video->getUserId();
$real_video_name = $video->getVideoName();
$real_date = $video->getDate();
$real_time = $video->getTime();
$real_description = $video->getDescription();
$real_friendy_description = $video->getFriendlyDescription();
$real_title = $video->getTitle();
// $real_friendy_title = $video->getFiendlyTitle();
$real_channel_id = $video->getChannelId();
$real_playlist_id = $video->getPlaylistId();
$real_status = $video->getStatus();
$real_ago = $video->getElpsTime();
$real_thumb = $video->getThumbnailPath();
$real_logo = $video->getLogoPath();
$real_is_Subbed = $reqId ? Subscription::isSub($_POST["user_id"], $video->getChannelId()) : False;

// $getID3 = new getID3;
// $path = $video->getPath();
// $file = $getID3->analyze($path);
// $resolution = [$file['video']["resolution_x"], $file['video']["resolution_y"]];
// if ($resolution[1] < 800) {
//     $low_res = False;
// } else {
//     $low_res = True;
// }
$real_low_res = isset($low_res) ? $low_res : False;

$realVideo = new NewVideo($real_id, $real_user_id, $real_video_name, $real_date,
        $real_time, $real_description, $real_title, $real_channel_id, 
        $real_playlist_id, $real_status, $real_likes, $real_dislikes, $real_views, $real_ago,
        $real_thumb, $real_Channel, $real_logo, $real_low_res, $real_is_Subbed, $real_friendy_description);
// var_dump($realVideo);
// die;
$appVideos[] = $realVideo;
}
// shuffle($appVideos);
header('Content-Type: application/json');
echo(json_encode($appVideos));
// var_dump(json_encode($appVideos));
// die;
}