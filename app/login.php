<?php

include_once '../classes/Connection.php';
include_once '../classes/User.php';
include_once '../helper.php';

$email = $_POST["email"];
$password = $_POST["password"];
class NewUser extends Connection{
	public $id, $lastname, $firstname, $email, $password, $logo, $date, $country, $status, $conn;

    function __construct($id, $lastname, $firstname, $email, $password, $logo, $date, $country, $status) {
        $this->id = $id;
        $this->lastname = $lastname;
        $this->firstname = $firstname;
        $this->email = $email;
        $this->logo = $logo;
        $this->status = $status;
        $this->date = $date;
        $this->country = $country;
        $this->password = $password;
        $this->conn = new Connection;
    }
}
if ($email && $password) {
    $data = User::getByEmail($email);
    if ($data) {
        $pass = password_verify($password, $data->getPassword());
        if ($pass) {
            $real_id = $data->getId();
		$real_lastname = $data->getLastname();
		$real_firstname = $data->getFirstname();
		$real_email = $data->getEmail();
		$real_password = $data->getPassword();
		$real_logo = $data->getLogo();
		$real_date = $data->getDate();
		$real_status = $data->getStatus();
		$real_country = $data->getCountry();

		$user = new NewUser($real_id, $real_lastname, $real_firstname, $real_email, $real_password,
		 $real_logo, $real_date, $real_country, $real_status);
        } else {
            echo("Wrong email or password!");
        }
    } else {
        echo("Wrong email or password!");
    }


	header('Content-Type: application/json');
	echo(json_encode($user));
}
