<?php

include_once '../classes/Connection.php';
include_once '../classes/User.php';
include_once '../classes/Video.php';
include_once '../classes/Thumbnail.php';
include_once '../classes/Channel.php';
include_once '../getID3/getid3/getid3.php';   

if(isset($_POST["user_id"])){
    $target_dir = UPLOAD_VIDEO_PATH;
    $uploadOkay = 1;
    $micro = explode(".",microtime())[1];
    $new_name = "VID_".$micro;
    $target_file = $target_dir.$new_name.'.'. pathinfo($_FILES["file"]["name"] ,PATHINFO_EXTENSION);

    if ($uploadOkay != 0 && move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
        $video_name = $new_name.'.'. pathinfo($_FILES["file"]["name"] ,PATHINFO_EXTENSION);
        
        if (isset($_FILES["thumb"])){
            $allowed_thumb_ext = ["png","jpg","jpeg"];
            $thumb_target_file = UPLOAD_THUMB_PATH."THUMB".$micro.".".pathinfo($_FILES["thumb"]["name"] ,PATHINFO_EXTENSION);
            $thumb_extension = strtolower(pathinfo($thumb_target_file, PATHINFO_EXTENSION));
            if (!in_array($thumb_extension, $allowed_thumb_ext)) {
                $thumb_uploadOkay = 0;
            }
            if ($uploadOkay != 0 && move_uploaded_file($_FILES["thumb"]["tmp_name"], $thumb_target_file)) {
                $thumbnail = "THUMB".$micro.".".pathinfo($_FILES["thumb"]["name"] ,PATHINFO_EXTENSION);
            }
        }else {
            $cmd_video = '"'.UPLOAD_VIDEO_PATH.$video_name.'"';
            $cmd_thumb = '"'.UPLOAD_THUMB_PATH.$new_name.".png".'"';
            $time = 5;
            $command = "ffmpeg -i $cmd_video -deinterlace -an -ss $time -f mjpeg -t 1 -r 1 -y -s 1280x720 $cmd_thumb";

            exec($command . ' 2>&1', $output);
            $thumbnail = $new_name.".png";

        }

        $thumb = isset($thumbnail) ? $thumbnail : "default.png";
        $title = strip_tags($_POST["title"]);
        $user_id = $_POST["user_id"];
        $description = isset($thumbnail) ? strip_tags($_POST["description"]) : isset($thumbnail);
        $user = User::getById($user_id);
        if ($user->getChannel()) {
            $channel_id = $user->getChannel()->getId();
        } else {
            $channel_id = null;
        }
        $date = date("Y/m/d");
        $time = date("H:i:s");

        $video = new Video($user_id, $video_name, $date, $time, $description, $title, $channel_id);
        //$real_thumbnail = new Thumbnail($name, $video_id);
        if ($video) {
            $video->add();
            $video_id = Video::getIdFromName($video_name);
            $real_thumbnail = new Thumbnail($thumb, $video_id);
            $real_thumbnail->add();
            echo"Video Uploaded Successfully";
        }else{
            echo "An Error has occured";
        }
        

    } else {
        if ($uploadOkay != 0) {
            echo"error";
        } else {
            echo"Wrong File Format";
        }
    }

}







    //$rename = rename(TEMP_PAYMENT_FILE_PATH.$month."_"."payment", PAYMENT_FILE_PATH.$month."_"."payment".$ext);
    // $file = fopen('log.txt', "a+", False);
// $content = $_POST;
//fwrite($file, $content);
//$file = fopen(PAYMENT_FILE_PATH.$month."_"."payment".$ext, "a", False);
//$content = "hey\n";
// fwrite($file, $content);
// fclose($file);
// include_once '../classes/Connection.php';
// include_once '../classes/User.php';
// include_once '../classes/Video.php';
// include_once '../classes/Thumbnail.php';
// include_once '../classes/Channel.php';
// include_once '../getID3/getid3/getid3.php';