<?php
include_once 'config.php';
//$cookie_user_id = "user";
//$cookie_value = "Alex Porter";
//setcookie("user", "alex", time() + (-86400 * 30), "/");
include_once 'partials/navbar.php';
include_once 'partials/sidebar.php';
?>
<div class="content d-inline-flex float-right">
    <div class="container mt-4">
        <div class="card">
            <div class="card-header p-2">
                <ul class="nav nav-pills">
                    <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab"><?= $_COOKIE["user_id"]?></a></li>
                    <li class="nav-item"><a class="nav-link" href="#channel" data-toggle="tab"></a></li>
                    <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab"></a></li>
                </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
                <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                        <!-- Post -->
                        <div class="container-fluid">
                            <div class="row">
                                <div class="d-none d-md-block col-md-1"></div>
                                <div class="col-md-4">
                                    <!-- Profile Image -->
                                    <div class="card card-primary card-outline h-100">
                                        <div class="card-body box-profile">
                                            <div class="text-center">
                                                <img class="profile-user-img img-fluid img-circle" src="<?= BASE_URL ?>images/nopics.jpg">
                                            </div>
                                            <h3 class="profile-username text-center">Nina Mcintire</h3>
                                            <p class="text-muted text-center">Software Engineer</p>
                                            <ul class="list-group list-group-unbordered mb-3">
                                                <li class="list-group-item">
                                                    <b>Followers</b> <a class="float-right">1,322</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Following</b> <a class="float-right">543</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Friends</b> <a class="float-right">13,287</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                </div>
                                <div class="d-none d-md-block col-md-2"></div>
                                <div class="d-none d-md-block col-md-4">
                                    <div class="card card-primary h-100">
                                        <div class="card-header">
                                            <h3 class="card-title">About Me</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <ul class="list-group list-group-flush mb-3">
                                                <li class="list-group-item">
                                                    <b>Name</b> <a class="float-right">Atoyebi AJibola</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Email</b> <a class="float-right">atoyebiajibola@email.com</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Channel</b> <a class="float-right">-</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Liked Videos</b> <a class="float-right">13,287</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Subcribed To</b> <a class="float-right">13,287</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Since</b> <a class="float-right">13,287</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                </div>
                                <div class="d-none d-md-block col-md-1"></div>
                            </div>
                        </div>
                        <!-- /.post -->
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="channel">
                        <!-- The channel -->
                        <div class="container">
                            <form>
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 col-form-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="name" placeholder="Email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">File input</label>
                                    <div class="col-sm-10">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="logo" onchange="displayName()">
                                            <label class="custom-file-label" id="logoVal"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-10">
                                        <label>Description</label>
                                        <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="settings">
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="inputName" placeholder="Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName2" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputName2" placeholder="Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputExperience" class="col-sm-2 col-form-label">Experience</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="inputExperience" placeholder="Experience"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputSkills" class="col-sm-2 col-form-label">Skills</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputSkills" placeholder="Skills">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                    <button type="submit" class="btn btn-danger">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div><!-- /.card-body -->
        </div>
         <!--/.nav-tabs-custom--> 
<!--<button type="button" class="btn btn-success toastrDefaultSuccess">
                  <?= $_SESSION["message"]?>
                </button>
        <button type="button" class="btn btn-success" onclick="flash('warning','hey')">
                  Launch Success Toast
                </button>-->
<div class="btn btn-danger" onclick="report('atoyebieniola93@gmail.com')">report</div>
    </div>
</div>

<?php include_once 'partials/footer.php'; ?>

