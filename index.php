<?php
include_once 'config.php';
include_once 'partials/navbar.php';
include_once 'partials/sidebar.php';
include_once 'classes/User.php';
?>
<div class="content d-inline-flex float-right" style="max-height: 90vh; overflow:auto">
    <div class="container-fluid bg-grey">
        <h4 class="m-3">Recommended</h4>
        <div class="row">
            <?php
            $videos = Video::getAllAvl();
            if (shuffle($videos)) {
                foreach ($videos as $video) {
                    include 'partials/home_row.php';
                }
            } else {
                echo '<div class="empty text-center"><h3>No Videos Yet :(</h3></div>';
            }
            ?>
        </div>
    </div>
</div>
<?php include_once 'partials/footer.php'; ?>