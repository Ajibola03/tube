<?php
include_once 'config.php';
include_once 'classes/Video.php';
include_once 'partials/navbar.php';
include_once 'partials/sidebar.php';

$videos = Video::getTrending();
?>
<div class="content mt-5 pt-5 d-inline-flex float-right">
    <div class="">
        <div class="container-fluid px-3 bg-grey">
            <div class="row">
                <div class="<?php
                if ($videos) {
                    echo"col-lg-9";
                } else {
                    echo"col-12";
                };
                ?> order-2 order-md-1">
                     <?php
                     if ($videos) {
                         foreach ($videos as $video) {
                             include 'partials/trending_block.php';
                         }
                     } else {
                         echo '<div class="empty"><h3>No Videos Yet :(</h3></div>';
                     }
                     ?>
                </div>
                <div class="col-md-3 bg-white order-1 order-md-2 side-full d-none d-lg-block">
                    <div class="mt-0">
                        <ul class="list-group-flush mt-3 p-0">
                            <li class="list-group-item"><b>Trending</b><b class="float-right">Views</b></li>
                            <?php
                            if ($videos) {
                                $i = 1;
                                foreach ($videos as $video) {
                                    if ($i > 10) {
                                        continue;
                                    }
                                    include 'partials/trending_line.php';
                                    $i++;
                                }
                            }
                            ?>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once 'partials/footer.php'; ?>
