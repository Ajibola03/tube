<?php
if (!isset($_SESSION)) {
    session_start();
}
// error_reporting(E_RECOVERABLE_ERROR | E_ERROR | E_CORE_ERROR | E_COMPILE_ERROR);
// ini_set("display_errors", true);
// error_reporting(E_ERROR);
include_once 'helper.php';
// include_once 'config.php';
function autoloadClass($className)
{
    $filename = "../classes/" . $className . ".php";
    if (is_readable($filename)) {
        require $filename;
    }
}
spl_autoload_register("autoloadClass");


define("EMAIL", "viewbloog@gmail.com");

define("PASS", "unthinkablepass");

define("BLOOGER_PER_VIEW", "0.024");

define("VIEWER_PER_VIEW", "0.040");

define("DS", DIRECTORY_SEPARATOR);

define("ADMIN_PASSWORD", "1");

define("BASE_URL", baseURL() . "/");

define("ADMIN_URL", BASE_URL . "admin/");

define("ADMIN_PATH", "D:\xampp\htdocs\tube\admin");


define("UPLOAD_URL", BASE_URL . "uploads/");

define("PAYMENT_FILE_URL", BASE_URL . "files/payments/");

define("UPLOAD_VIDEO_URL", BASE_URL . "uploads/videos/");

define("UPLOAD_THUMB_URL", BASE_URL . "uploads/thumbs/");

define("UPLOAD_VIDEO_PATH", __DIR__. DS . "uploads" . DS . "videos" . DS);

define("UPLOAD_LOGO_PATH", __DIR__. DS . "uploads" . DS . "logo" . DS);

define("UPLOAD_PROFILE_PATH", __DIR__. DS . "uploads" . DS . "profiles" . DS);

define("UPLOAD_THUMB_PATH", __DIR__. DS . "uploads" . DS . "thumbs" . DS);

define("PAYMENT_FILE_PATH", __DIR__. DS . "files" . DS . "payments" . DS);

define("TEMP_PAYMENT_FILE_PATH", __DIR__. DS . "controllers" . DS);

define("FFMPEG_PATH", __DIR__. DS . "plugins" . DS . "ffmpeg-0.5.15");

define("THUMBS_PATH", __DIR__. DS . "uploads" . DS . "thumbs" . DS);

define("THUMBS_NETWORK_PATH", BASE_URL."uploads/thumbs/");

define("PROFILE_NETWORK_PATH", BASE_URL."uploads/profiles/");

// var_dump(__DIR__);
// die;

//define("UPLOAD_RAW_PATH", UPLOAD_PATH."raw".DS);
//
//define("UPLOAD_RAW_URL", BASE_URL."uploads/raw/");
//
//define("UPLOAD_50_PATH", UPLOAD_PATH."50".DS);
//
//define("UPLOAD_50_URL", BASE_URL."uploads/50/");
//
//define("UPLOAD_100_PATH", UPLOAD_PATH."100".DS);
//
//define("UPLOAD_100_URL", BASE_URL."uploads/100/");
//
//define("UPLOAD_250_PATH", UPLOAD_PATH."250".DS);
//
//define("UPLOAD_250_URL", BASE_URL."uploads/250/");