<?php
include_once 'config.php';
include_once 'partials/navbar.php';
include_once 'partials/sidebar.php';
?>
<div class="content d-inline-flex float-right">
    <div class="container mt-4">
         <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                    <i class="fas fa-cog mr-1" style="color:blue"></i>
                  Setting
                </h3>
                <div class="card-tools">
                  <ul class="nav nav-pills ml-auto">
                    <li class="nav-item">
                      <a class="nav-link active" href="#generalSetting" data-toggle="tab">General</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#sales-chart" data-toggle="tab">Donut</a>
                    </li>
                  </ul>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content p-0">
                  <!-- Morris chart - Sales -->
                  <div class="chart tab-pane active" id="generalSetting"
                       style="position: relative; height: 300px;">
                      <canvas id="revenue-chart-canvas" height="300" style="height: 300px;"></canvas>                         
                   </div>
                  <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;">
                    <canvas id="sales-chart-canvas" height="300" style="height: 300px;"></canvas>                         
                  </div>  
                </div>
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
    </div>
</div>

<?php include_once 'partials/footer.php'; ?>
        <!--<div class="btn btn-danger" onclick="report('atoyebieniola5@gmail.com')">report</div>-->

