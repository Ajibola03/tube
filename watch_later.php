<?php
include_once 'config.php';
include_once 'helper.php';
include_once 'partials/navbar.php';
include_once 'partials/sidebar.php';
$i = 0;
$videos = [];
//setcookie("watch_list", 10, time() + (-86400 * 30), "/");
//die;
if (isset($_COOKIE["watch_list"])) {
    $i = count(json_decode($_COOKIE["watch_list"]));
    foreach (json_decode($_COOKIE["watch_list"]) as $id) {
        $videos[] = Video::getById($id);
    }
}
?>
<div class="content d-inline-flex float-right bg-grey">
    <div class="container-fluid bg-grey">
        <div class="row pl-3">
            <div class="col-12" id="later_block">
                <div class="container-fluid bg-grey padded">
                    <div class="row" style="border-bottom: 2px solid #ededed;">
                        <div class="col-sm-6">
                            <h6 class="m-3"><i class="fa fa-clock"></i> Watch Later</h6>
                        </div>
                        <div class="container-fluid">
                            <div class="row">
                                <?php
                                if (count($videos) > 0) {
                                    foreach ($videos as $video) {
                                        echo "<br>";
                                        include 'partials/later_block.php';
                                    }
                                } else {
                                    echo '<div class="text-center">No Videos Here</div>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 text-center py-4">
                <div class="btn btn-sm btn-danger" onclick="clearWatchLater()">Clear Watch Later</div>
            </div>
        </div>
    </div>
    <?php include_once 'partials/footer.php'; ?>