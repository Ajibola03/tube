<div class="col-12">
    <div class="row">
        <div class="col-1">
            <img src="<?= BASE_URL ?>images/nopics.jpg" style="width:100%" class="rounded-circle">
        </div> 
        <div class="col-11">
            <p class="m-0 mb-1"><b><?= $reply->getUser() ?></b> - <span><?= $reply->getTimeDiff() ?></span></p>
            <p class="m-0"><?= $reply ?></p>
            <p style='color:grey'>@<?= $reply->getAtName() ?></p>
            <div class="d-none" id="comment-block<?= $reply->getId() ?>" class="d-none">
                <textarea autofocus="true" class="form-control" id="comment<?= $reply->getId() ?>" onclick="$('#buttons-block<?= $reply->getId() ?>').removeClass('d-none')">
            
                </textarea>
                <div class="float-right my-2 d-none" id="buttons-block<?= $reply->getId() ?>">
                    <div class="btn btn-default" onclick="flash('danger', 'cancelled')">Cancel</div>
                    <div class="btn btn-primary" 
                         onclick="comment(<?= $_SESSION['user_id'] ?>,<?= $video->getId() ?>, document.getElementById('comment<?= $reply->getId() ?>').value, type = '2', at = '<?= $comment->getUserId() ?>')">Submit</div>
                </div>
            </div>
            <div>
                <div class="btn btn-sm">
                    <i class="fa fa-thumbs-up"></i>
                </div>
                <div class="btn btn-sm">
                    <i class="fa fa-thumbs-down"></i>
                </div>
                <div class="btn btn-sm" onclick="$('#comment-block<?= $reply->getId() ?>').removeClass('d-none'); $('#buttons-block<?= $reply->getId() ?>').removeClass('d-none')">
                    <i class="fa fa-reply"></i>
                </div>
            </div>
        </div>
    </div>
</div>