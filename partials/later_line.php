<div class="container-fluid bg-grey padded">
    <div class="row" style="border-bottom: 2px solid #ededed padded">
        <div class="col-sm-6"><h6 class="m-3"><i class="fa fa-clock"></i> Watch Later</h6></div>
        <div class="col-sm-6 text-right">Showing <span><?= $i ?></span> of <span><?= $i ?></span> Videos</div>
        <?php
        foreach ($videos as $video) {
            include '../partials/later_block.php';
        }
        ?>
    </div>
</div>