<!--NB:want to remove icon text on toggle-->
<div class="side-bar d-block">
    <ul>
        <a href="<?= BASE_URL ?>">
            <li class="">
                <i class="fa fa-home icon px-2"></i>
                <pre class="d-inline large pl-0">Home</pre>
            </li>
        </a>
        <a href="<?= BASE_URL ?>trending">
            <li class="<?php if ($current_page === "trending.php") echo "active" ?>">
                <i class="fa fa-chart-line icon px-2"></i>
                <pre class="d-inline large pl-0">Trending</pre>
            </li>
        </a>
        <a href="<?= BASE_URL ?>sub">
            <li class="<?php if ($current_page === "sub.php") echo "active" ?>">
                <i class="far fa-play-circle icon px-2"></i>
                <pre class="d-inline large pl-0">Subscriptions</pre>
            </li>
        </a><hr>
        <a href="<?= BASE_URL ?>history">
            <li class="<?php if ($current_page === "history.php") echo "active" ?>">
                <i class="fa fa-history icon px-2"></i>
                <pre class="d-inline large pl-0">History</pre>
            </li>
        </a>
        <a href="<?= BASE_URL ?>watch_later">
            <li class="<?php if ($current_page === "watch_later.php") echo "active" ?>">
                <i class="fa fa-clock icon px-2"></i>
                <pre class="d-inline large pl-0">Watch later</pre>
            </li>
        </a>
        <a href="<?= BASE_URL ?>liked">
            <li class="<?php if ($current_page === "liked.php") echo "active" ?>">
                <i class="fa fa-heart icon px-2"></i>
                <pre class="d-inline large pl-0">Liked Videos</pre>
            </li>
        </a>
        <a href="<?= BASE_URL ?>money">
            <li class="<?php if ($current_page === "money.php") echo "active" ?>">
                <i class="fa fa-money-bill-alt icon px-2"></i>
                <pre class="d-inline large pl-0">My Money</pre>
            </li>
        </a><hr>
        <a href="<?= BASE_URL ?>account_">
            <li class="<?php if ($current_page === "account_.php") echo "active" ?>">
                <i class="fa fa-user-circle icon px-2"></i>
                <pre class="d-inline large pl-0">My Channel</pre>
            </li>
        </a>
        <a href="<?= BASE_URL ?>setting">
            <li class="<?php if ($current_page === "setting.php") echo "active" ?>">
                <i class="fa fa-cog icon px-2"></i>
                <pre class="d-inline large pl-0">Settings</pre>
            </li>
        </a>
        <a href="">
            <li>
                <i class="fa fa-question-circle icon px-2"></i>
                <pre class="d-inline large pl-0">Help</pre>
            </li>
        </a><hr>
    </ul>
</div>
<div class="d-none shadow" onclick="hideSidebar()">

</div>
<div class="d-none shadow-blur" onclick="$('.search_results').addClass('d-none');$('.shadow-blur').addClass('d-none')">

</div>