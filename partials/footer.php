<div id="signUpModal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary text-center">
                <div class="modal-tile">
                    <h3 class="text-center text-white" style="text-align: center"><i class="fa fa-key mr-3" style="color:white;"></i>Sign Up</h3>
                </div>
            </div>
            <div class="modal-body">
                <form action="<?= BASE_URL ?>controllers/sign_up.php" method="post" enctype="multipart/form-data">
                    <div class="container-fluid">
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="email">Last Name:</label>
                                    <input type="name" class="form-control star" placeholder="Enter Last Name" name="lastname">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="email">First Name:</label>
                                    <input type="text" class="form-control star" placeholder="Enter First Name" name="firstname">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="pwd">Email:</label>
                                    <input type="email" class="form-control star" placeholder="Enter email" name="email">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Country</label>
                                    <select class="form-control select2 star" style="width: 100%;" name="country">
                                        <option selected="selected">Alabama</option>
                                        <option>Alaska</option>
                                        <option>California</option>
                                        <option>Delaware</option>
                                        <option>Tennessee</option>
                                        <option>Texas</option>
                                        <option>Washington</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="exampleInputFile">Profile Picture:</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="profile" name="profile" onchange="displayName('profile', 'profilename')">
                                        <label class="custom-file-label" for="exampleInputFile" id="profilename">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-pass">
                                    <label for="pwd">Password:</label>
                                    <input type="password" class="form-control star" placeholder="Enter password" id="password" name="password" value="">
                                </div>
                                <div class="m-0 text-danger d-none validate-length text-center fz-sm">
                                    <p>Password Must Be at Least Characters Long</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-pass">
                                    <label for="pwd">Confirm Password:</label>
                                    <input type="password" class="form-control" placeholder="Retype password" id="password2" name="password2">
                                </div>
                                <div class="m-0 text-danger d-none validate text-center fz-sm">
                                    <p>Password Mismatch</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-primary" id="sub">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<div id="loginModal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary text-center">
                <div class="modal-tile">
                    <h3 class="text-center text-white" style="text-align: center"><i class="fa fa-key mr-3" style="color:white;"></i>Login</h3>
                </div>
            </div>
            <div class="modal-body">
                <form action="<?= BASE_URL ?>controllers/login.php" method="post">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="pwd">Email:</label>
                                    <input type="email" class="form-control star" placeholder="Enter email" id="log_email" name="email">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="pwd">Password:</label>
                                    <input type="password" class="form-control star" placeholder="Enter password" id="log_password" name="password" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<div id="uploadModal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary text-center">
                <div class="modal-tile">
                    <h3 class="text-center text-white" style="text-align: center"><i class="fa fa-cloud-upload-alt mr-3" style="color:white;"></i>Upload Video</h3>
                </div>
                <div class="close" data-target='#uploadModal' data-dismiss='modal'><i class="fa fa-times"></i></div>
            </div>
            <div class="modal-body">
                <form action="<?= BASE_URL ?>controllers/upload_video.php" method="post" enctype="multipart/form-data" id="myForm">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="exampleInputFile">Video:</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input star" id="video" name="video" onchange="displayName('video', 'videoname')">
                                        <label class="custom-file-label" for="exampleInputFile" id="videoname">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Title:</label>
                                    <input type="text" class="form-control star" placeholder="Enter video Title" id="title" name="title">
                                </div>
                            </div>
                            <div id="percentage"> </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" rows="3" placeholder="Please Enter Description..." name="description"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-12 d-none">
                                <div class="form-group">
                                    <label>Title:</label>
                                    <input type="text" class="form-control star" value="<?= $location ?>" id="location" name="location">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-primary" id="upload" value="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<!-- jquery -->
<script src="<?= BASE_URL ?>dist/js/jquery-3.4.1.js"></script>
<!-- popper -->
<script src="<?= BASE_URL ?>dist/js/popper.min.js"></script>
<!-- bootstrap -->
<script src="<?= BASE_URL ?>bs.v4/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?= BASE_URL ?>plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="<?= BASE_URL ?>plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- SweetAlert2 -->
<script src="<?= BASE_URL ?>plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?= BASE_URL ?>plugins/toastr/toastr.min.js"></script>
<!-- Moment -->
<script src="<?= BASE_URL ?>plugins/toastr/toastr.min.js"></script>
<script src="<?= BASE_URL ?>plugins/moment/moment.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= BASE_URL ?>plugins/admin/js/adminlte.js"></script>
<script type="text/javascript" src="<?= BASE_URL ?>dist/js/mdb.min.js"></script>
<!-- personal js -->
<script src="<?= BASE_URL ?>dist/js/main.js"></script>
<?php if ($current_page == "play.php") { ?>
    <!--VideoJs-->
    <script src="<?= BASE_URL ?>node_modules\videojs-vtt.js\distvtt.js"></script>
    <script src="<?= BASE_URL ?>node_modules\video.js\dist\video.js"></script>
    <script>
        //  videojs.options.flash.swf = "http://example.com/path/to/video-js.swf"
    </script>
<?php } ?>
<script>
    $(function() {

        toTitle(<?= json_encode($current_page) ?>)
    });

    <?php if ($current_page == "play.php") { ?>
        $(document).ready(function() {
            setTimeout(function() {
                $(".play").click();
            }, 3000);
            videojs('example_video_1', {
                userActions: {
                    doubleClick: false
                },
            });



        });
    <?php } ?>
</script>
<?php flash("error"); ?>
</body>

</html>