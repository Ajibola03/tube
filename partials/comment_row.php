<div class="col-12">
    <div class="row">
        <div class="col-1 my-auto">
            <img src="<?= BASE_URL ?>uploads/profiles/<?= $comment->getProfilePic() ?>" style="width:80%" class="rounded-circle">
        </div> 
        <div class="col-11 mt-3">
            <p class="m-0 mb-1"><b><?= $comment->getUser() ?></b> - <span><?= $comment->getTimeDiff() ?></span></p>
            <p class="m-0"><?= $comment ?></p>
            <?php
            if ($comment->getType() == 2) {
                $replies = $comment->getAllReplies();
            }
            ?>
            <div class="d-none" id="comment-block<?= $comment->getId() ?>" class="d-none">
                <textarea autofocus="true" class="form-control comment" id="comment<?= $comment->getId() ?>" onclick="$('#buttons-block<?= $comment->getId() ?>').removeClass('d-none')">
            
                </textarea>
                <div class="float-right my-2 d-none" id="buttons-block<?= $comment->getId() ?>">
                    <div class="btn btn-danger" onclick="$('#comment-block<?= $comment->getId() ?>').addClass('d-none')">Cancel</div>
                    <div class="btn btn-primary" 
                         onclick="comment(<?= $_SESSION['user_id'] ?>,<?= $video->getId() ?>, document.getElementById('comment<?= $comment->getId() ?>').value, type = '2', at = '<?= $comment->getId() ?>')">Submit</div>
                </div>
            </div>
            <div>
                <div class="btn btn-sm">
                    <i class="fa fa-thumbs-up"></i>
                </div>
                <div class="btn btn-sm">
                    <i class="fa fa-thumbs-down"></i>
                </div>
                <div class="btn btn-sm" onclick="$('#comment-block<?= $comment->getId() ?>').removeClass('d-none'); $('#buttons-block<?= $comment->getId() ?>').removeClass('d-none')">
                    <i class="fa fa-reply"></i>
                </div>
                <div class="btn btn-sm" onclick="$('#replies<?= $comment->getId() ?>').toggleClass('d-none'); $(this).html().split('<div')[0].trim() === 'see replies' ? $(this).html('hide replies') : $(this).html('see replies');">
                    see replies
                </div>
            </div>
        </div>
        <div class="ml-5 p-2 d-none" id="replies<?= $comment->getid() ?>" style="border-left: 1px solid grey">
            <div class="row">
            <?php
            $replies = $comment->getAllReplies();
            foreach ($replies as $reply) {
                include 'comment_reply_row.php';
            }
            ?>
        </div>
        </div>
    </div>
</div>