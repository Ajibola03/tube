<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label>Title:</label>
                <input type="text" class="form-control star" id="title" value="<?= $video->getTitle() ?>" >
            </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" rows="3" id="description"><?= $video->getDescription() ?></textarea>
            </div>
        </div>
        <div class="col-sm-6 d-none">
            <div class="form-group">
                <label>Title:</label>
                <input type="text" class="form-control star" id="id" value="<?= $_SESSION["user_id"] ?>">
            </div>
        </div>
        <div class="m-0 text-danger d-none validate-login text-center"><p>Password Must Be at Least Characters Long</p></div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-sm btn-primary" onclick="editVideo(<?= $i ?>,<?= $video->getId() ?>)">Submit</button>