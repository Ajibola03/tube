<tr id="user<?= $i?>">
    <td><?= $i ?></td>
    <td><?= $user->getId() ?></td>
    <td><?= $user->getLastname() ?></td>
    <td><?= $user->getFirstname() ?></td>
    <td><?= $user->getEmail() ?></td>
    <td><?= $user->getChannelName() ?></td>
    <td><?= $user->getStatus() ?></td>
    <td><?= $user->getStatusBadge() ?></td>
    <td><?= $user->getDate() ?></td>
    <td><?= $user->getCountry() ?></td>
    <td>
        <div class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-bell"></i>
                <span class="badge badge-warning navbar-badge">15</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <a href="#" class="dropdown-item" onclick="loadEditUser(<?= $i?>,<?= $user->getId()?>)">
                    <i class="fas fa-pencil mr-2"></i> Edit
                    <span class="float-right text-muted text-sm">3 mins</span>
                </a>
                <a href="#" class="dropdown-item">
                    <i class="fas fa-envelope mr-2"></i> 4 new messages
                    <span class="float-right text-muted text-sm">3 mins</span>
                </a>
            </div>
        </div>
    </td>
</tr>