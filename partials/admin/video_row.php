<tr id="video<?= $i?>">
    <td><?= $i ?></td>
    <td><?= $video->getId() ?></td>
    <td><?= $video->getUser() ?></td>
    <td><?= $video->getTitle() ?></td>
    <td><?= $video->getFriendlyDescription() ?></td>
    <td><?= $video->getChannelName() ?></td>
    <td><?= $video->getViews() ?></td>
    <td><?= $video->getStatusBadge() ?></td>
    <td><?= $video->getDate() ?></td>
    <td>
        <div class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-bell"></i>
                <span class="badge badge-warning navbar-badge">15</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <a href="#" class="dropdown-item" onclick="loadEditVideo(<?= $i?>,<?= $video->getId()?>)">
                    <i class="fas fa-pencil mr-2"></i> Edit
                    <span class="float-right text-muted text-sm">3 mins</span>
                </a>
                <a href="#" class="dropdown-item">
                    <i class="fas fa-envelope mr-2"></i> 4 new messages
                    <span class="float-right text-muted text-sm">3 mins</span>
                </a>
            </div>
        </div>
    </td>
</tr>