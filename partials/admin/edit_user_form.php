<!--<form action="<?= BASE_URL ?>controllers/edit_user.php" method="post" enctype="multipart/form-data">-->
    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-6">
                <div class="form-group">
                    <label for="email">Last Name:</label>
                    <input type="name" class="form-control" id="lastname" name="lastname" value="<?= $user->getLastName()?>">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="email">First Name:</label>
                    <input type="text" class="form-control" id="firstname" name="firstname" value="<?= $user->getFirstName()?>">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="pwd">Email:</label>
                    <input type="email" class="form-control" id="email" name="email" value="<?= $user->getEmail()?>">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Country</label>
                    <select class="form-control select2 mySelect2" style="width: 100%;" id="country" name="country" value="<?= $user->getCountry()?>">
                        <option>Alabama</option>
                        <option>Alaska</option>
                        <option>California</option>
                        <option>Delaware</option>
                        <option>Tennessee</option>
                        <option>Texas</option>
                        <option>Washington</option>
                    </select>
                    <script type="text/javascript">
                        $('.mySelect2').select2();
                    </script>

                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-sm btn-primary" id="submit" onclick="editUser(<?= $i?>,<?= $user->getId()?>)">Submit</button>
<!--</form>-->