<?php
//session_unset();
//die;
$URL = $_SERVER['PHP_SELF'];
$current_page = explode('/', $URL);
$current_page = strtolower(end($current_page));
include __DIR__.'../../../config.php';
// if ($current_page == "index.php") {
//     // include_once '../config.php';
//     // include_once '../helper.php';
//     // include_once '../classes/User.php';
//     // include_once '../classes/Video.php';
//     // include_once '../classes/Channel.php';
// } else {
//     // include_once '../../config.php';
//     // include_once '../../helper.php';
//     // include_once '../../classes/User.php';
//     // include_once '../../classes/Video.php';
//     // include_once '../../classes/Channel.php';
// }
$users = User::getAll();
$new_users = User::getNew();
$videos = Video::getAll();
$channels = Channel::getAll();
$no_of_videos = count($videos);
$no_of_channels = count($channels);
$no_of_users = count($users);
$no_of_new_users = count($new_users);
//var_dump(date("D"));
//echo '<br>';
//var_dump($no_of_new_users);
//die;
if (!isset($_SESSION["admin_id"])){
    header("location: /admin/login");
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>AdminLTE 3 | Dashboard 2</title>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= BASE_URL ?>fontawesome/css/all.min.css">
        <link rel="stylesheet" href="<?= BASE_URL ?>fontawesome/css/brands.min.css">
        <link rel="stylesheet" href="<?= BASE_URL ?>fontawesome/css/regular.min.css">
        <link rel="stylesheet" href="<?= BASE_URL ?>fontawesome/css/svg-with-js.css">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?= BASE_URL ?>bs.v4/css/bootstrap.min.css">
        <!-- DataTables -->
        <link rel="stylesheet" href="<?= BASE_URL ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="<?= BASE_URL ?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="<?= BASE_URL ?>plugins/select2/css/select2.min.css">
        <link rel="stylesheet" href="<?= BASE_URL ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
        <!-- Bootstrap4 Duallistbox -->
        <link rel="stylesheet" href="<?= BASE_URL ?>plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= BASE_URL ?>plugins/admin/css/adminlte.min.css">
        <!-- Toastr -->
        <link rel="stylesheet" href="<?= BASE_URL ?>plugins/toastr/toastr.min.css">
         <!-- <link rel="stylesheet" href=vendor/select2/dist/css/select2.css"> -->
    </head>
    <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
        <div class="wrapper">
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="index3.html" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="#" class="nav-link">Contact</a>
                    </li>
                </ul>

                <!-- SEARCH FORM -->
                <form class="form-inline ml-3">
                    <div class="input-group input-group-sm">
                        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                        <div class="input-group-append">
                            <button class="btn btn-navbar" type="submit">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>

                <!-- Right navbar links -->
                <ul class="navbar-nav ml-auto">
                    <!-- Notifications Dropdown Menu -->
                    <li class="nav-item dropdown">
                        <a class="nav-link" data-toggle="dropdown" href="#">
                            <i class="far fa-bell"></i>
                            <span class="badge badge-warning navbar-badge">15</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                            <span class="dropdown-item dropdown-header">15 Notifications</span>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item">
                                <i class="fas fa-envelope mr-2"></i> 4 new messages
                                <span class="float-right text-muted text-sm">3 mins</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item">
                                <i class="fas fa-users mr-2"></i> 8 friend requests
                                <span class="float-right text-muted text-sm">12 hours</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item">
                                <i class="fas fa-file mr-2"></i> 3 new reports
                                <span class="float-right text-muted text-sm">2 days</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                        </div>
                    </li>
                </ul>
            </nav>