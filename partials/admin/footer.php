<div id="editUserModal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary text-center">
                <div class="modal-tile">
                    <h3 class="text-center text-white" style="text-align: center"><i class="fa fa-key mr-3" style="color:white;"></i>Sign Up</h3>
                </div>
            </div>
            <div class="modal-body" id="editUser">

            </div>
        </div>
    </div>
</div>
</div>
</div>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
<!-- Main Footer -->
<footer class="main-footer">
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 3.0.2
    </div>
</footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jquery -->
<script src="<?= BASE_URL ?>dist/js/jquery-3.4.1.js"></script>
<!-- popper -->
<script src="<?= BASE_URL ?>dist/js/popper.min.js"></script>
<!-- Bootstrap -->
<script src="<?= BASE_URL ?>bs.v4/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="<?= BASE_URL ?>plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="<?= BASE_URL ?>plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- SweetAlert2 -->
<script src="<?= BASE_URL ?>plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?= BASE_URL ?>plugins/toastr/toastr.min.js"></script>
<!-- DataTables -->
<script src="<?= BASE_URL ?>plugins/datatables-bs4/js/jquery.dataTables.min.js"></script>
<script src="<?= BASE_URL ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?= BASE_URL ?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= BASE_URL ?>plugins/admin/js/adminlte.js"></script>
<!-- OPTIONAL SCRIPTS -->
<script src="<?= BASE_URL ?>plugins/admin/js/demo.js"></script>
<!-- ChartJS --> 
<script src="<?= BASE_URL ?>plugins/chart.js/Chart.min.js"></script>
<script src="<?= BASE_URL ?>dist/js/main.js"></script> 
<script>
    function flash(type, message) {
        if (type && message) {
            if (type === "success") {
                toastr.success(message);
            } else if (type === "info") {
                toastr.info(message);
            } else if (type === "danger") {
                toastr.error(message);
            } else if (type === "error") {
                toastr.error(message);
            } else if (type === "warning") {
                toastr.warning(message);
            } else {
                toastr.warning(message);
            }
        }
    }
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
        //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
        $("a.nav-link.active").parents($("li.has-treeview:eq(0)")).addClass("menu-open").children($("a.nav-link:eq(0)")).addClass("active")
    });
</script>
<?php flash("error"); ?>
</body>
</html>
