<div class="card mt-5">
    <div class="card-header">
        <h3 class="card-title">Money Details </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
                <div class="row">
                    <div class="col-12 col-sm-4">
                        <div class="info-box bg-light">
                            <div class="info-box-content">
                                <span class="info-box-text text-center text-muted">Total Amount Extracted</span>
                                <span class="info-box-number text-center text-muted mb-0"><?= $money->getTotalAmountExtracted() ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="info-box bg-light">
                            <div class="info-box-content">
                                <span class="info-box-text text-center text-muted">Amount Available ($)</span>
                                <span class="info-box-number text-center text-muted mb-0"><?= $money->getAmountAvl() ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="info-box bg-light">
                            <div class="info-box-content">
                                <span class="info-box-text text-center text-muted">Number Of Videos</span>
                                <span class="info-box-number text-center text-muted mb-0"><?= $user->getNoOfVideos() ?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h4 class="mb-3">Top Earning Videos</h4>
                        <div class="btn btn-sm btn-primary float-right" >Show More</div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
                <img src="<?= BASE_URL ?>images/nopics.jpg" class="rounded-circle w-50" style="margin-left: 25%"><br>
                <div class="text-muted">
                    <p class="text-sm"><?php
                        $user->getChannel() ? $val = 'Channel Name' : $val = 'Name';
                        echo $val;
                        ?>
                        <b class="d-block"><?= $user->getChannelName() ?></b>
                    </p>
                </div>
                <div class="text-center mt-5 mb-3">
                    <div class="btn btn-sm btn-primary <?= getExtractString() ?>" onclick="addToExtract(<?= $money->getId() ?>)">Extract</div>
                    <?php if (getDaysLeftVal()) { ?>
                    <a href="#" class="btn btn-sm btn-warning"><?= getDaysLeft() ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
</div>