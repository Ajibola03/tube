<div class="row pt-5">
    <div class="col-md-12">
        <center>
            <img src="<?= BASE_URL ?>uploads/profiles/<?= $user->getLogo() ?>" alt="Circle Image" class="img-raised rounded-circle img-fluid" style="width: 20%;">
            <!-- <img src="<?// BASE_URL ?>uploads/profiles/<? //$channel->getLogo() ?>" alt="Circle Image" class="img-raised rounded-circle img-fluid" style="width: 20%;"> -->
        </center>
    </div>
</div>
<div class="description text-center mt-5">
    <div class="name mt5">
        <h3 class="title"><?= $user ?></h3>
        <h6>Designer</h6>
    </div>
    <p>An artist of considerable range, Chet Faker — the name taken by Melbourne-raised, Brooklyn-based Nick Murphy — writes, performs and records all of his own music, giving it a warm, intimate feel with a solid groove structure. </p>
</div>
<div class="row">
    <div class="col-md-6 ml-auto mr-auto">
        <div class="profile-tabs">
            <ul class="nav nav-pills nav-pills-icons justify-content-center" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#videos" role="tab" data-toggle="tab">
                        <i class="fa fa-play-circle pt-3"></i>
                        <span class="ml-2" style="vertical-align: top">Videos</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#playlists" role="tab" data-toggle="tab">
                        <i class="fa fa-layers"></i>
                        <span class="ml-2" style="vertical-align: top">Playlist</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#favorite" role="tab" data-toggle="tab">
                        <i class="fa fa-heart"></i>
                        <span class="ml-2" style="vertical-align: top">Favorite</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="tab-content tab-space">
    <div class="tab-pane active text-center gallery" id="videos">
        <div class="row">
            <!--<div class="col-md-3 ml-auto">-->
            <div class="container mx-sm-5">
                <div class="row">
                    <?php
                    if ($videos) {
                        foreach ($videos as $video) {
                            include 'partials/chn_vid_row.php';
                        }
                    } else {
                        echo 'No videos';
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-3 mr-auto">

            </div>
        </div>
    </div>
    <div class="tab-pane text-center gallery" id="playlists">
        <div class="row">
            <?php
            if ($playlists) {
                foreach ($playlists as $playlist) {
                    include 'partials/chn_vid_row.php';
                }
            } else {
                echo '<h6 class="d-block text-center col-12">No Playlist</h6>';
            }
            ?>
        </div>
    </div>
    <div class="tab-pane text-center gallery" id="favorite">
        <div class="row">
            <!--<div class="col-md-3 ml-auto">-->
               <h6 class="d-block text-center col-12">No Favorites</h6>;
            <!--</div>-->
<!--            <div class="col-md-3 mr-auto">
                
            </div>-->
        </div>
    </div>
</div>
