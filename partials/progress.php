<?php

$key = ini_get("session.upload_progress.prefix") . $_POST["form"];
if (!empty($_SESSION[$key])) {
    $current = $_SESSION[$key]["bytes_processed"];
    $total = $_SESSION[$key]["content_length"];
    return $current < $total ? ceil($current / $total * 100) : 100;
}
else {
    var_dump($key);
    die;
}