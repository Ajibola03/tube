<div class="card mt-5">
    <div class="card-header">
        <h3 class="card-title"><i class="fa fa-ban"></i> No Account</h3>
    </div>
    <div class="card-body">
        <div class="my-5">You do not have a money account you can open one now :)</div>
        <div><div class="btn btn-sm btn-primary btn-block" data-toggle="modal" data-target="#modal">Create</div></div>
    </div>
    <!-- /.card-body -->
</div>
