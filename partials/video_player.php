<?php
include_once '../classes/Subscription.php';
$channel = false;
if ($video->getChannel()) {
    $channel = Channel::getById($video->getChannelId());
}
//var_dump($channel);
//die;
?>
<div class="container-fluid">
    <div class="d-inline-block float-left body" style="width:80%">
        <div id="vid">
            <video width="100%" id="player" playsinline  controls autoplay loop onplay="startAdTimer()">
                <source id="playerSrc" src="<?= UPLOAD_VIDEO_URL ?>ad.mp4" type="video/mp4">
            </video>
            <div class="text-right skip d-none">
                <div class="btn btn-sm btn-dark timer"><span id="timeleft">20</span>s to money</div><br>
                <div class="btn btn-sm btn-dark" onclick="changeVid('<?= $video ?>')"><i class="fa fa-angle-right mx-3"></i>skip ad</div>
            </div>
        </div>
        <div style="max-width: 100%" class="">
            <div class="py-2"><h4>Title</h4></div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-3">
                                <img src="<?= BASE_URL ?>images/nopics.jpg" style="width:100%" class="rounded-circle">
                            </div>
                            <div class="col-9">
                                Channel Name<br>
                                <?php
                                if ($channel) {
                                    echo $channel->getLikes();
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
//                        var_dump($video);
//                        die;
                    ?>
                    <div class="col-sm-4 details">
                        <span><?= $video->getViews() ?> views</span>
                        <?php
                        if ($user) {
                            include_once '../partials/user_options.php';
                        } else {
                            include_once '../partials/no_user_options.php';
                        }
                        ?>
                    </div>
                    <?php
                    if ($channel && $user) {
                        if (Subscription::getSub($_SESSION["user_id"], $video->getChannelId())) {
                            include_once '../partials/subscribe_button2.php';
                        } else {
                            include_once '../partials/subscribe_button.php';
                        }
                    }
                    ?>
                    <!--<div class="btn btn-sm btn-dark" onclick="displayTime('player')"></div>-->

                </div>

            </div>
        </div>
        <div class="video-overlay d-none p-5" style="width:100%" onmouseleave="hideOverlay()">
            <span><i class="fa fa-angle-left float-left"></i></span>
            <span><i class="fa fa-bars float-right"></i></span>
            <div class="text-center" style="position: relative; top:50%;">
                <span><i class="fa fa-angle-left mx-5"></i></span>
                <span><i class="fa fa-play mx-5"></i></span>
                <span><i class="fa fa-arrow-right mx-5"></i></span>
            </div>
            <div class="text-right" style="position: relative; top:70%;">
                <div class="btn btn-sm btn-dark">20s to money</div>
            </div>
            <div class="text-right" style="position: relative; top:75%;">
                <div class="btn btn-sm btn-dark"><i class="fa fa-angle-left mx-3"></i></div>
            </div>
        </div>
    </div>
    <div class="d-inline-block float-right" style="width:20%">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="m-2 mt-0">
                        <img src="<?= BASE_URL ?>images/mqdefault_6s_480x270.webp" style="width:100%" >
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="m-2">
                        <img src="<?= BASE_URL ?>images/mqdefault_6s_480x270.webp" style="width:100%" >
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="m-2">
                        <img src="<?= BASE_URL ?>images/mqdefault_6s_480x270.webp" style="width:100%" >
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>