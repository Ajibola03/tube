<style>
    .container2 {
        position: relative;
    }
    .topleft {
        position: absolute;
        top: 8px;
        left: 16px;
        font-size: 18px;

    }
    .center {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        font-size: 18px;
    }
    .bottomLeft {
        position: absolute;
        bottom: 20px;
        left: 16px;
        font-size: 18px;
    }
    .bottomRight {
        position: absolute;
        bottom: 100px;
        right: 16px;
        /*font-size: 18px;*/
    }
    video {
    }
    .video-js .vjs-time-control {
    display: block;
}
.video-js .vjs-remaining-time {
    display: none;
}
.video-js .vjs-play-progress {
  background-color: blue;
}
</style>
<div class="container2">
    <video id="example_video_1" class="video-js vjs-16-9 vjs-fluid vjs-default-skin vjs-big-play-centered" controls autoplay
           controlsList="nodownload" onplay="startAdTimer()" onended="changeVid('<?= $_GET["id"] ?>','<?= json_encode($low_res) ?>,<?= $ref_id ?>')">
        <source id="playerSrc" src="<?= UPLOAD_VIDEO_URL ?>ad.mp4" type="video/mp4">
    </video>
    <div class="text-right skip d-none bottomRight">
        <div class="btn btn-sm btn-dark timer"><span id="timeleft">20</span>s to money</div><br>
        <div class="btn btn-sm btn-dark" onclick="changeVid('<?= $_GET["id"] ?>', '<?= json_encode($low_res) ?>'), addView(<?= $video->getId() ?>,<?= $ref_id ?>)"><i class="fa fa-angle-right mx-3"></i>skip ad</div>
    </div>
</div>