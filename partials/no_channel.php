<div class="">
    <div id="notfound">
        <div class="notfound">
            <div class="notfound-404">
                <h1><span></span></h1>
            </div>
            <h2>Oops! You Do Not Have A Channel</h2>
            <p>Sorry you do not have a channel, you can create one now :)</p>
            <span class=" btn link py-3" data-toggle="modal" data-target="#createChannelModal">Create Channel</span>
            <span><a href="<?= BASE_URL ?>">Back to homepage</a></span>
        </div>
    </div>
</div>
<div id="createChannelModal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary text-center">
                <div class="modal-tile">
                    <h3 class="text-center text-white" style="text-align: center"><i class="fa fa-play-circle mr-3" style="color:white;"></i>Create Channel</h3>
                </div>
            </div>
            <div class="modal-body">
                <?php
                if (isset($_SESSION["user_id"])) {
                    include_once 'partials/create_channel_form.php';
                } else {
                    echo "<h5 class='text-center text-danger'>You need to login to create channel</h5>";
                }
                ?>
            </div>
        </div>
    </div>
</div>