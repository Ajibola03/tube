<div class="history-block container-fluid">
    <div class="row">
        <div class="col-lg-8 col-sm-10">
            <a href="<?= BASE_URL ?>play?id=<?= $video->getId() ?>">
                <div class="row">
                    <div class="col-sm-4 p-0 m-0">
                        <img src="<?= UPLOAD_THUMB_URL.$video->getThumbnail() ?>" style="width: 100%">
                    </div>
                    <div class="d-inline-block text col-sm-8">
                        <h6><?= $video->getTitle() ?></h6>
                        <span>Liverpool FC</span><span><?= $video->getElpsTime() ?></span>
                        <span class="col-1 dot pr-2 d-sm-none d-block float-right <?php if (!isset($_SESSION["user_id"])) echo 'd-none2'; ?>" style="z-index:3;">
                            <i class="dot-item" data-toggle="dropdown" href="#">
                                <i class="fa fa-ellipsis-v float-right" style="color:black"></i>
                            </i>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                <i class="dropdown-item" onclick="addToWatchLater(<?= $video->getId() ?>)">
                                    Add To Watch Later
                                </i>
                                <i class="dropdown-item">
                                    Delete
                                </i>
                            </div>
                        </span>
                        <p class="d-none d-xl-block"><?= $video->getFriendlyDescription() ?></p>
                    </div>
                </div>
            </a>
        </div>
<!--        <div class="col-2 dot d-none d-sm-block<?php if(!isset($_SESSION["user_id"])) echo 'd-none2';?>" style="z-index:3">
            <i class="dot-item" data-toggle="dropdown" href="#">
                <i class="fa fa-ellipsis-v float-right" style="color:black"></i>
            </i>
            <div class="dropdown-menu">
                <i class="dropdown-item" onclick="">
                    Remove From History
                </i>
                <i class="dropdown-item" onclick="addToWatchLater(<?= $video->getId()?>)">
                    Add To Watch Later
                </i>
                <i class="dropdown-item">
                    Delete
                </i>
            </div>
        </div>-->
    </div>
</div>