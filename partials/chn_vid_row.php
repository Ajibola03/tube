<style type="text/css">
    .dot.home .fa{
        cursor:pointer;
        position: absolute;
        bottom: 7%;
        right: 25px;
    }
    .dot .fa:hover{
        color: red !important;
    }
    .dot.home{
        position: absolute;
        /*bottom: 8%;*/
        right: 25px;
    }
    .avatar.changed{
        margin: 0 !important;
    }
    a, a:hover{
        text-decoration: none;
        color:black;
    }
    pre{
        overflow: hidden; 
        /*font-size: 95% !important;*/
    }
    pre.large{
        /*overflow: hidden; */
        font-size: 95% !important;
    }
    .title-pre, pre.title{
        /*overflow: hidden;*/
/*        margin-right: .75rem !important;
        font-size: 90% !important;*/
    }
</style>
<div class="col-md-4 p-3">
    <a href="<?= BASE_URL ?>play?id=<?= $video->getId() ?>" class="rel-link" style="position: relative; left: 0">
        <div class="m-2">
            <img src="<?= BASE_URL ?>uploads/thumbs/<?= $video->getThumbnail() ?>" style="width:100%" >
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-1 p-0">
                    <img src="<?= BASE_URL ?>uploads/profiles/<?= $video->getLogo() ?>" class="avatar changed"> 
                </div>
                <div class="col-11 fz-sm pr-1">
                    <pre class="m-0 pt-0 title-pre" title="<?= $video->getTitle() ?>"><b><?= $video->getTitle() ?></b></pre>
                </div>
                <div class="col-12 pr-1">
                    <?= $video->getChannelName() . " " ?>
                    <i class="fa fa-check-circle verification fz-xs"></i>
                    <?= $video->getElpsTime() ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </a>
    <span class="dot home <?php if (!isset($_SESSION["user_id"])) echo 'd-none'; ?>">
        <i class="" data-toggle="dropdown" href="#">
            <i class="fa fa-ellipsis-v" style="color:grey" onclick=""></i>
        </i>
        <div class="dropdown-menu dropdown-menu-left">
            <i class="dropdown-item" onclick="addToWatchLater(<?= $video->getId() ?>)">
                Add To Watch Later
            </i>
            <i class="dropdown-item" onclick="deleteVideo(15, <?= $video->getId() ?>)">
                Delete
            </i>
        </div>
    </span>
</div>