<style>
    .container2 {
        position: relative;
    }
    .topleft {
        position: absolute;
        top: 8px;
        left: 16px;
        font-size: 18px;

    }
    .center {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        font-size: 18px;
    }
    .bottomLeft {
        position: absolute;
        bottom: 20px;
        left: 16px;
        font-size: 18px;
    }
    .bottomRight {
        position: absolute;
        bottom: 100px;
        right: 16px;
        /*font-size: 18px;*/
    }
    video {
    }
    .video-js .vjs-time-control {
    display: block;
}
.video-js .vjs-remaining-time {
    display: none;
}
.video-js .vjs-play-progress {
  background-color: blue;
}
</style>
<div class="container2" style="max-width: 100% !important; margin: auto; background-color: black">
    <video width="100%" id="example_video_1" class="video-js vjs-16-9 vjs-fluid vjs-default-skin vjs-big-play-centered" playsinline  controls autoplay loop onplay="addView(<?= $video->getId() ?>,<?= $ref_id ?>)">
        <source id="playerSrc" src="<?= UPLOAD_VIDEO_URL ?><?= $video ?>" type="video/mp4">
    </video>
    <div class="btn btn-dark play d-none" onclick="document.getElementById('player').play()">play</div>
</div>