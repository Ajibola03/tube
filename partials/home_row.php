<div class="col-md-3 p-3">
    <a href="<?= BASE_URL ?>play?id=<?= $video->getId() ?>" class="rel-link" style="position: relative">
        <div class="m-2">
            <img src="uploads/thumbs/<?= $video->getThumbnail() ?>" style="width:100%" >
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-1 p-0">
                    <img src="uploads/profiles/<?= $video->getLogo() ?>" class="avatar"> 
                </div>
                <div class="col-11 fz-sm pr-1">
                    <pre class="m-0 pt-0 title-pre" title="<?= $video->getTitle() ?>"><b><?= $video->getTitle() ?></b></pre>
                </div>
                <div class="col-12 pr-1">
                    <?= $video->getChannelName() . " " ?>
                    <i class="fa fa-check-circle verification fz-xs"></i>
                    <?= $video->getElpsTime() ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </a>
    <span class="dot home <?php if (!isset($_SESSION["user_id"])) echo 'd-none'; ?>">
        <i class="" data-toggle="dropdown" href="#">
            <i class="fa fa-ellipsis-v" style="color:grey" onclick=""></i>
        </i>
        <div class="dropdown-menu ">
            <i class="dropdown-item" onclick="addToWatchLater(<?= $video->getId() ?>)">
                Add To Watch Later
            </i>
            <i class="dropdown-item">
                Delete
            </i>
        </div>
    </span>
</div>

