<div class="trending-block col-xl-6">
<div class="container-fluid px-3">
    <div class="row">
        <div class="col-sm-10 px-3">
            <a href="<?= BASE_URL ?>play.php?id=<?= $video->getId() ?>">
                <div class="row">
                    <div class="col-sm-4 p-0 m-0">
                        <img src="<?= UPLOAD_THUMB_URL.$video->getThumbnail() ?>" style="width: 100%">
                    </div>
                    <div class="d-inline-block text col-sm-8">
                        <h6><?= $video->getTitle() ?></h6>
                        <span><?= $video->getChannelName()?></span><span><?= $video->getViews() ?>Views</span><span><?= $video->getElpsTime() ?></span>
                        <p title="<?= $video->getDescription() ?>"><?= $video->getFriendlyDescription() ?></p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-1 dot <?php if(!isset($_SESSION["user_id"])) echo 'd-none2';?> d-none d-sm-block" style="z-index:3">
            <i class="dot-item" data-toggle="dropdown" href="#">
                <i class="fa fa-ellipsis-v float-right" style="color:black"></i>
            </i>
            <div class="dropdown-menu">
                <i class="dropdown-item" onclick="addToWatchLater(<?= $video->getId()?>)">
                    Add To Watch Later
                </i>
                <i class="dropdown-item">
                    Delete
                </i>
            </div>
        </div>
    </div>
</div>
</div>