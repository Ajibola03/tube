<form action="<?= BASE_URL ?>controllers/create_channel.php" method="post" enctype="multipart/form-data">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control star" placeholder="Enter Channel Name" name="name">
                </div>
            </div>
            <div class="form-group col-sm-6">
                <label for="exampleInputFile">Logo:</label>
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="logo"  name="logo" onchange="displayName('logo', 'logoname')">
                        <label class="custom-file-label" for="exampleInputFile" id="logoname" value="">Choose file</label>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label for="choice">Do you wish to add your previous videos to this channel:</label>
                    <input type="checkbox" class="" name="choice">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-sm btn-primary">Submit</button>
            </form>
        </div>
    </div>