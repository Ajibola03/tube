<div id="regMoneyModal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary text-center">
                <div class="modal-tile">
                    <h3 class="text-center text-white" style="text-align: center"><i class="fa fa-key mr-3" style="color:white;"></i>Register</h3>
                </div>
                <div class="close" data-target="#regMoneyModal" data-dismiss="modal"><i class="fa fa-times"></i></div>
            </div>
            <div class="modal-body">
                <form action="<?= BASE_URL ?>controllers/add_money_user.php" method="post" enctype="multipart/form-data" id="myForm">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group col-sm-6 d-none">
                                <label>User_id:</label>
                                <div class="input-group">
                                        <input type="text" class="form-control star" id="userId"  name="userId" value="<?= $_SESSION["user_id"]?>">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Paypal:</label>
                                    <input type="text" class="form-control star" placeholder="Enter PayPal Account" id="paypal" name="paypal">
                                </div>
                            </div>
                            <div class="col-sm-6 d-none">
                                <div class="form-group">
                                    <label>type:</label>
                                    <input type="text" class="form-control star" id="type" name="type" value="<?= $type ?>">

                                    <div id="status"></div>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-primary" id="" onclick="">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>