<?php
include_once 'config.php';
include_once 'helper.php';
include_once 'classes/User.php';
include_once 'classes/Video.php';
include_once 'classes/View.php';
include_once 'classes/Subscription.php';
include_once 'classes/Like.php';
include_once 'classes/Dislike.php';
include_once 'classes/Channel.php';
include_once 'classes/Guest.php';
include_once 'classes/Money.php';

//setcookie("watch_list", 10, time() + (-86400 * 30), "/");
//die;
//setcookie("user_id", 10, time() + (-86400 * 30), "/");
//setcookie("guest", 10, time() + (-86400 * 30), "/");
//var_dump($_COOKIE);
//die;
ob_start('minify_html');
$URL = $_SERVER['PHP_SELF'];
$current_page = explode('/', $URL);
$current_page = strtolower(end($current_page));
$location = BASE_URL.explode(".php", $current_page)[0];
$new_guest = False;
$array = ["200","100", "500", 300];
//var_dump(array_remove($array, 200));
//die;
//check for Guest and Type Of Guest;
if (!isset($_COOKIE["user_id"]) && !isset($_COOKIE["guest"])) {
    setcookie("guest", 0, time() + (86400 * 30), "/");
    $new_guest = True;
    $_SESSION["guest"] = True;
} else if (!isset($_COOKIE["user_id"]) && isset($_COOKIE["guest"])) {
    $_SESSION["guest"] = True;
} else if (isset($_COOKIE["user_id"]) && isset($_COOKIE["guest"])) {
    setcookie("guest", 0, time() + (-86400 * 30), "/");
    $_SESSION["user_id"] = $_COOKIE["user_id"];
} else {
    $_SESSION["guest"] = False;
    $logged_in = True;
    $_SESSION["user_id"] = $_COOKIE["user_id"];
    $user = True;
}
//Add Guest
if ($new_guest) {
    $guestObj = new Guest(date("Y/m/d"));
    $guest = $guestObj->add();
}
//Create user Object and channel
if (!isset($_SESSION["guest"])) {
    $user = User::getById($_SESSION["user_id"]);
    if ($user->getChannel()) {
        $channel = Channel::getByUserId($_SESSION["user_id"]);
    }
} else {
    $user = False;
    $channel = False;
}

if (isset($_SESSION["user_id"])){
    $user = User::getById($_SESSION["user_id"]);
}

if (isset($_SESSION["video_id"])) {
    if ($current_page !== "play.php") {
        $_SESSION["video_id"] = -1;
    }
}

if (isset($_SESSION["update_money"])) {
    if ($_SESSION["update_money"] == True) {
        $money = Money::getByUserId($_SESSION["user_id"]);
        $tempuser = User::getById($_SESSION["user_id"]);
        $money->setChannel_id($tempuser->getChannel()->getId());
        if ($money->update()) {
//            flash("success", "updated");
            $_SESSION["update_money"] = False;
        }
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title id="titlebar">Bloog View |</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= BASE_URL ?>fontawesome/css/all.min.css">
        <link rel="stylesheet" href="<?= BASE_URL ?>fontawesome/css/brands.min.css">
        <link rel="stylesheet" href="<?= BASE_URL ?>fontawesome/css/regular.min.css">
        <link rel="stylesheet" href="<?= BASE_URL ?>fontawesome/css/svg-with-js.css">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?= BASE_URL ?>bs.v4/css/bootstrap.min.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="<?= BASE_URL ?>plugins/select2/css/select2.min.css">
        <link rel="stylesheet" href="<?= BASE_URL ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
        <!-- Bootstrap4 Duallistbox -->
        <link rel="stylesheet" href="<?= BASE_URL ?>plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= BASE_URL ?>plugins/admin/css/adminlte.min.css">
        <!-- Toastr -->
        <link rel="stylesheet" href="<?= BASE_URL ?>plugins/toastr/toastr.min.css">
        <!--Videojs-->
        <link href="<?= BASE_URL ?>node_modules\video.js\dist\video-js.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?= BASE_URL ?>dist/css/mdb.min.css">
        <!-- My Css -->
        <link rel="stylesheet" href="<?= BASE_URL ?>dist/css/main.css">
        <link href="<?= BASE_URL ?>images/icon.png" rel="icon" type="image/png" />
        <?php 
            if($current_page == "account_.php"){
                echo '
                    <link rel="stylesheet" type="text/css" href="'.BASE_URL.'plugins/account_/css/account_.css">
    <link rel="stylesheet" type="text/css" href="'.BASE_URL.'plugins/account_/css/notfound.css">
    <link href="'.BASE_URL.'images/icon.png" rel="icon" type="image/png" />
                ';
            }
        ?>
    </head>
    <body class="bg-grey">
        <div class="nav-bar py-3 px-4">
            <div class="row">
                <div class="col-sm-3 d-none d-lg-block">
                    <span class="text-left svg bars d-none" style="font-size:20px"  onclick="toggleSidebar()"><i class="fas fa-arrow-right pl-1 pb-2"></i></span>
                    <span class="text-left svg bars" style="font-size:20px"  onclick="toggleSidebar()"><i class="fas fa-arrow-left"></i></span>
                    <img src="<?= BASE_URL ?>images/logo.jpg" class="img-fluid d-none d-lg-inline" style="width:50%" />
                </div>
                <div class="col-6  d-lg-none">
                    <span class="text-left svg bars" style="font-size:20px"  onclick="toggleSidebar()"><i class="fas fa-arrow-right pl-1 pb-2"></i></span>
                    <span class="text-left svg bars d-none" style="font-size:20px"  onclick="toggleSidebar()"><i class="fas fa-arrow-left"></i></span>
                    <img src="<?= BASE_URL ?>images/logo.jpg" class="img-fluid d-lg-none" style="width:50%" />
                </div>
                <div class="col-6 px-5 d-none d-lg-block">
                        <input type="text" class="gen-search myin" placeholder="Search" onfocus="search('1')" onkeyup="search('1')" id="search1" onbur="$('.search_results').addClass('d-none')">
                        <div class="myout"><span class="rounded well ">search</span></div>
                    <div class="elevated card bg-white search_results p-3 d-none w-hin" style="border:1px solid black;">
                        <div class="container">
                            <div class="card-header">
                                <h5 id="topic1" class="card-title">Search Results for </h5>
                            </div>
                            <div id="result1" class="card-body" style="max-height: 600px !important; overflow-y: scroll">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="hidden-search d-none">
                    <input type="text" class="myin myin-sm gen-search" placeholder="Search" onfocus="search('2')" onkeyup="search('2')" id="search2">
                    <div class="myout"><span class="rounded well ">search</span></div>
                    <div class="elevated card bg-white search_results p-3 d-none col-12" style="border:1px solid black;">
                        <div class="container">
                            <div class="card-header">
                                <h5 id="topic2" class="card-title">Search Results for </h5>
                            </div>
                            <div id="result2" class="card-body" style="max-width: 20px !important">

                            </div>
                        </div>
                    </div>
                </div>
                <div style="vertical-align: top" class="col-6 col-lg-3 text-right options <?php
                    if (!isset($_SESSION["user_id"])) {
                        echo 'd-none';
                    }
                    ?>">
                     <?php include 'partials/logged_in_options.php'; ?>
                </div>
                <div class="col-6 <?php
                     if (isset($_SESSION["user_id"])) {
                         echo 'd-none';
                     }
                     ?> col-lg-3 text-right options" style="vertical-align: top">
                    <div class="row my-1 d-none d-sm-block">
                        <?php include 'partials/login_sign_up.php'; ?>
                    </div>
                    <div class="d-sm-none">
                        <i class="fa fa-angle-down toggle-hide" onclick="$('.hidden-option').toggleClass('d-none'); $('.toggle-hide').toggleClass('d-none')"></i>
                        <i class="fa fa-angle-down toggle-hide d-none"onclick="$('.hidden-option').toggleClass('d-none'); $('.toggle-hide').toggleClass('d-none')"></i>
                    </div>
                </div>
                <div class="col-12 d-none text-right options hidden-option" style="vertical-align: top">
                    <div class="row">
                        <?php include 'partials/login_sign_up.php'; ?>
                    </div>
                </div>
            </div>
        </div>