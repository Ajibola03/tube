<?php
include_once 'config.php';
include_once 'partials/navbar.php';
include_once 'partials/sidebar.php';
include_once 'classes/User.php';
?>
<div class="content d-inline-flex float-right">
    <div class="container-fluid bg-grey">
        <div class="row">
            <div class="">
                <div class="container-fluid padded">
                    <div class="row" style="border-bottom: 2px solid #ededed" class="padded">
                        <div class="col-sm-6"><h6 class="m-3"><i class="fa fa-heart"></i> Liked</h6></div>
                        <div class="col-sm-6 text-right"></div>
                        <?php
                        if (isset($_SESSION["user_id"])) {
                            $videos = Video::getLiked($_SESSION["user_id"]);
                            if ($videos) {
                                foreach ($videos as $video) {
                                    include 'partials/liked_block.php';
                                }
                            } else {
                                echo 'You have no liked videos';
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include_once 'partials/footer.php'; ?>

