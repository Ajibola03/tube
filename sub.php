<?php

// include_once 'helper.php';
include_once 'config.php';
include_once 'partials/navbar.php';
include_once 'partials/sidebar.php';

$videos = False;
if (isset($_SESSION["user_id"])) {
    $user = User::getById($_SESSION["user_id"]);
    $check = Subscription::getByUserId($_SESSION["user_id"]);
    $channels = $user->getSubscribedChannels();
    $videos = Subscription::getVideos($channels);
}
?>
<div class="content d-inline-block float-right sub">
    <div class="container-fluid bg-grey">
        <div class="row padded pl-3">
            <?php
            if ($videos) {
                foreach ($videos as $video) {
                    include 'partials/sub_block.php';
                }
            } else {
                echo '<div class="empty text-center"><h5>Subscribe to a channel to see their latest videos here</h5></div>';
            }
            ?>
        </div>
    </div>
</div>
<?php include_once 'partials/footer.php'; ?>

